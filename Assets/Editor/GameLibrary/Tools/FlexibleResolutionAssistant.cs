﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace GameLibrary.UI {

    internal class FlexibleResolutionAssistant {
        public bool isFlexible;

        [MenuItem("Tools/Flexible Resolution/Fit To Rect", true)]
        public static bool CanSetFlexible() {
            return Selection.activeGameObject != null;
        }

        [MenuItem("Tools/Flexible Resolution/Fit To Rect")]
        public static void SetFlexible() {
            GameObject gameObject = Selection.activeGameObject;

            RectTransform rect = gameObject.transform as RectTransform;
            RectTransform global = gameObject.transform.parent as RectTransform;
            /*pivot's position*/
            float w = global.rect.width;
            float h = global.rect.height;

            float left = rect.localPosition.x - rect.rect.width * rect.pivot.x + w * global.pivot.x;
            float minX = left / w;
            float right = rect.localPosition.x + rect.rect.width * (1 - rect.pivot.x) + w * global.pivot.x;
            float maxX = right / w;

            float top = rect.localPosition.y - rect.rect.height * rect.pivot.y + h * global.pivot.y;
            float minY = top / h;
            float bottom = rect.localPosition.y + rect.rect.height * (1 - rect.pivot.y) + h * global.pivot.y;
            float maxY = bottom / h;

            rect.anchorMin = new Vector2(minX, minY);
            rect.anchorMax = new Vector2(maxX, maxY);

            rect.offsetMin = new Vector2(0, 0);
            rect.offsetMax = new Vector2(0, 0);
        }
    }
}

#endif