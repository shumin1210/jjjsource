﻿using UnityEditor;
using UnityEngine;

namespace GameLibrary.Utility {

    [CustomEditor(typeof(VariantElement))]
    public class VariantElementEditor : Editor {

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save setting")) {
                (target as VariantElement).ApplySetting();
            }
            GUILayout.EndHorizontal();
        }
    }
}