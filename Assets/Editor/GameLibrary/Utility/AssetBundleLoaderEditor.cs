﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameLibrary.Utility {

    [CustomEditor(typeof(AssetBundleLoader))]
    public class AssetBundleLoaderEditor : Editor {

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            //GUILayout.BeginHorizontal();
            //GUILayout.Button("");
        }
    }
}