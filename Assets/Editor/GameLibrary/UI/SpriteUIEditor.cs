﻿using UnityEditor;
using UnityEngine;

namespace GameLibrary.UI {

    /// <summary>SpriteUI的編輯器擴充</summary>
    [CustomEditor(typeof(SpriteUI))]
    [CanEditMultipleObjects]
    public class SpriteUIEditor : Editor {

        public override void OnInspectorGUI() {
            SpriteUI ui = target as SpriteUI;
            serializedObject.Update();
            DrawDefaultInspector();

            if (GUILayout.Button("Reload Sprite")) {
                ui.LoadSprite();
            }
        }
    }
}