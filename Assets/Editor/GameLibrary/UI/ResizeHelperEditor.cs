﻿using UnityEngine;
using UnityEditor;

namespace GameLibrary.UI {
    [CustomEditor(typeof(ResizeHelper))]
    [CanEditMultipleObjects]
    public class ResizeHelperEditor : Editor {
        public override void OnInspectorGUI() {
            ResizeHelper ui = target as ResizeHelper;
            serializedObject.Update();
            DrawDefaultInspector();

            if (GUILayout.Button("Reset Size")) {
                ui.ResetSize();
            }
        }
    }
}