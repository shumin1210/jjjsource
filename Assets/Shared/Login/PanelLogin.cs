﻿using GameLibrary.Net;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelLogin : MonoBehaviour {
    private const string KEY_CACHED = "login_cache";
    private const string KEY_ACCOUNT = "login_account";
    private const string KEY_PASSWORD = "login_password";

    [SerializeField]
    private MsgHandler Handler;

    private bool cacheLoginInfo = false;

    [SerializeField]
    private Image BG;

    [SerializeField]
    private Image Window;

    [SerializeField]
    private Image Logo;

    [SerializeField]
    private InputField AccInput;

    [SerializeField]
    private InputField PwdInput;

    [SerializeField]
    private Toggle CacheInfo;

    [SerializeField]
    private Image Button;

    private string account;
    private string password;

    private void Start() {
        cacheLoginInfo = PlayerPrefs.GetInt(KEY_CACHED, 0) == 1;
        CacheInfo.isOn = cacheLoginInfo;
        account = PlayerPrefs.GetString(KEY_ACCOUNT, "");
        AccInput.text = account;
        password = PlayerPrefs.GetString(KEY_PASSWORD, "");
        PwdInput.text = password;

        DefaultResManager.SetFont(gameObject);
    }

    public void Login() {
        Handler.StartConnecting("login");
        string acc = AccInput.text;
        string pwd = PwdInput.text;
        SaveLoginInfo(acc, pwd);
        Handler.Send("login", new {
            acc,
            pwd
        });
    }

    public void CacheLoginInfoChange(bool value) {
        cacheLoginInfo = value;
        PlayerPrefs.SetInt(KEY_CACHED, cacheLoginInfo ? 1 : 0);
        if (!cacheLoginInfo) {
            DeleteLoginInfo();
        }
    }

    private void SaveLoginInfo(string acc, string pwd) {
        PlayerPrefs.SetString(KEY_ACCOUNT, acc);
        PlayerPrefs.SetString(KEY_PASSWORD, pwd);
        PlayerPrefs.Save();
    }

    private void DeleteLoginInfo() {
        PlayerPrefs.DeleteKey(KEY_ACCOUNT);
        PlayerPrefs.DeleteKey(KEY_PASSWORD);
    }

    private void Update() {
        var system = EventSystem.current;
        if (Input.GetKeyDown(KeyCode.Tab)) {
            Selectable next = system.currentSelectedGameObject.GetComponent<InputField>().FindSelectableOnDown();

            if (next == null || !(next is InputField)) {
                var list = system.currentSelectedGameObject.GetComponents<InputField>();
                if (list.Length > 0) {
                    next = list[0];
                }
            }
            if (next != null) {
                InputField inputfield = next.GetComponent<InputField>();
                if (inputfield != null) {
                    inputfield.OnPointerClick(new PointerEventData(system));
                }
                system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
            }
        }
    }

#if UNITY_EDITOR

    [UnityEditor.MenuItem("Test/Login")]
    public static void loginAsLxc11() {
        DataShared.Instance.GetData<MsgHandler>(MsgHandler.KEY)
            .Send("login", new {
                acc = "lxc10",
                pwd = "111111"
            });
    }

#endif
}