﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[ExecuteInEditMode]
public class LoadingScene : MonoBehaviour {
    public Slider ProgressBar;
    public string Text;
    public Text TxtStatus;
    public float progress;

    public void Update() {
        if (ProgressBar && ProgressBar.value != progress) {
            ProgressBar.value = progress * 100;
        }
        if (TxtStatus) {
            TxtStatus.text = Text;
        }
    }

    public void LoadScene(string name) {
    }
}