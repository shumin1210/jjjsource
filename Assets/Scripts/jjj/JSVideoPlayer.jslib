﻿mergeInto(LibraryManager.library, {
    PlayVideo: function (id, src) {
        Factorys.SWFPlayer.GetInstance(Pointer_stringify(id)).Play(Pointer_stringify(src));
    },
    ToggleVideoPlayer: function (id, on) {
        Factorys.SWFPlayer.GetInstance(Pointer_stringify(id)).Toggle(on);
    },
    PauseVideoPlayer: function (id) {
        Factorys.SWFPlayer.GetInstance(Pointer_stringify(id)).Pause();
    },
    ResumeVideoPlayer: function (id) {
        Factorys.SWFPlayer.GetInstance(Pointer_stringify(id)).Resume();
    },
    SetVideoPlayerRect: function (id, x, y, w, h) {
        Factorys.SWFPlayer.GetInstance(Pointer_stringify(id)).SetRect(x, y, w, h);
    }
});