﻿using System.Collections.Generic;
using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;

namespace JJJ {

    public abstract class GameLobby : MonoBehaviour {

        /// <summary>現在顯示廳與牌桌資料的GID</summary>
        public string GID;

        /// <summary>暫存已進入的GID,換桌功能時使用</summary>
        public string ChangeGID = "";

        /// <summary>通訊實體</summary>
        protected JJJHandler handler;

        #region GameObjects

        [SerializeField]
        private GameObject PrefabGameSession;

        private List<LobbyGameSession> Sessions = new List<LobbyGameSession>();

        [SerializeField]
        private PanelGame panelGame;

        [SerializeField]
        protected IGameScreen GameScreen;
        
        [SerializeField]
        protected GameObject PanelGameSession;

        #endregion GameObjects

        #region 待實作方法

        /// <summary>初始化:選廳UI</summary>
        protected void buildGameSessions() {
            List<IDictionary<string, object>> gss = DynamicParser.ToDynamicList(DataShared.Instance.GetData("gamelist.Games"));
            if (gss == null) {
                return;
            }
            /*gamesession buttons*/
            int i = 1;
            foreach (IDictionary<string, object> item in gss) {
                LobbyGameSession g;
                if (Sessions.Count > i - 1) {
                    g = Sessions[i - 1];
                } else {
                    g = GameObjectRelate.InstantiateGameObject(PanelGameSession, PrefabGameSession)
                        .GetComponent<LobbyGameSession>();
                    g.parent = this;
                    Sessions.Add(g);
                }

                g.name = "gamesession" + i;
                g.GID = item["GID"].ToString();
                g.No = i;
                g.SetSelected(i == 1);
                g.SetText();
                g.gameObject.SetActive(true);
                DefaultResManager.SetFont(g);
                i++;
            }
            for (; i - 1 < Sessions.Count; i++) {
                Sessions[i - 1].gameObject.SetActive(false);
            }
        }

        /// <summary>畫面更新:選聽UI</summary>
        protected void refreshGameSessionBtn() {
            foreach (LobbyGameSession item in GetComponentsInChildren<LobbyGameSession>()) {
                item.SetSelected(item.GID == GID);
            }
        }

        /// <summary>切換到指定的廳資料</summary>
        /// <param name="gid">指定的GID</param>
        public void SwitchGameSession(string gid) {
            GID = gid;
            Refresh();
        }

        #endregion 待實作方法

        protected void Start() {
            handler = DataShared.Instance.GetData<JJJHandler>(JJJHandler.HandlerID);
            Refresh();
        }

        protected void OnEnable() {
            buildGameSessions();
            Refresh();
        }

        /// <summary>設定啟用或隱藏物件,啟用同時更新畫面</summary>
        /// <param name="on">true為啟用</param>
        public void SetActive(bool on) {
            gameObject.SetActive(on);
            Refresh();
        }
        
        /// <summary>指令:進入牌桌</summary>
        /// <param name="feeType">是否有抽傭</param>
        public void Enter(bool feeType) {
            if (!string.IsNullOrEmpty(ChangeGID) && ChangeGID != GID) {
                /*cross change session*/
                ChangeGID = GID;

            }
            if (!string.IsNullOrEmpty(ChangeGID)) {
                handler.Send("leave", new {
                    GID = ChangeGID
                });
            }

            GameSession gs = getGameSession();
            handler.StartConnecting("enter");
            handler.Send("enter", new {
                GID = gs.GID,
                FeeType = feeType ? 1 : 0
            });
        }

        /// <summary>取消換桌</summary>
        public virtual void Cancel() {
            if (!string.IsNullOrEmpty(ChangeGID)) {
                GameScreen.holdForChangeTable = false;
                gameObject.SetActive(false);
                GameScreen.SetActive(true);
            }
        }

        /// <summary>取得使用中的廳資料</summary>
        /// <returns>廳資料物件</returns>
        protected GameSession getGameSession() {
            return DataShared.Instance.GetData<GameSession>("GameSessions." + GID);
        }

        /// <summary>畫面更新:全部</summary>
        public void Refresh() {
            buildGameSessions();
            refreshGameSessionBtn();

            GameSession gs = getGameSession();
            if (gs == null) {
                return;
            }
            
        }
        
    }
}