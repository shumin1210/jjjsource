﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChipBeted : MonoBehaviour {
    public long Value;

    public void Set(long value, Sprite sprite = null) {
        gameObject.SetActive(true);
        Value = value;

        if (sprite != null) {
            gameObject.GetComponent<Image>().sprite = sprite;
        }
    }

    public void SetZero() {
        gameObject.SetActive(false);
        Value = 0;
    }
}