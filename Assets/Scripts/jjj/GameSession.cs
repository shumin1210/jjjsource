﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Utility;
using UnityEngine;

namespace JJJ {

    public class GameSession {
        public bool occupied = false;

        public string GID;
        public int Number;
        public string VideoURL;
        public string Round = "00-00";

        public bool IsClosing;
        public int AccessLevel = 0;

        public int ViewerCount = 0;
        public int OrderCount = 0;
        public bool isPlayering = false;
        
        public GameState State;
        public DateTime ActTime;
        public DateTime EndTime;
        public long Limit;

        public int Seat;
        public long Gain;
        public long Bet;
        
        public long DurationCheck_ms;
        public long DurationDeal_ms = 700;

        public ResultData Result;

        public GameSession() {
            
        }
        
        public void CancelBet() {
            
        }

        public void Restart() {
            CancelBet();
        }

        public string GetCommandKey(CommandBtnKey key) {
            switch (key) {
                case CommandBtnKey.Grab:
                    return Command.Grab;
                case CommandBtnKey.Left:
                    //if (VideoDisplay == VideoDisplay.Camera1)
                        return Command.Left;
                    //else
                    //    return Command.Down;
                case CommandBtnKey.Right:
                    //if (VideoDisplay == VideoDisplay.Camera1)
                        return Command.Right;
                    //else
                    //    return Command.Up;
                default:
                    return Command.None;    
            }            
        }
    }
}