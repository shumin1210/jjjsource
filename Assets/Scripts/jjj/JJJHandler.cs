﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Core;
using GameLibrary.Net;
using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;

namespace JJJ {

    public class JJJHandler : MsgHandler {
        public const string GID = "jjj";
        public new static string HandlerID = "jjj_handler";

        //[SerializeField]
        //private GameObject[] PanelList;

        public JJJHandler() {
            RegistryHandler("login",
                Receive: msg => {
                    //DataShared.Instance.SetData("gamelist", msg);
                    DataShared.Instance.SetData(KEYS.USER, new Player() {
                        Name = msg["Name"].ToString(),
                        //Credit = Convert.ToInt64(msg["UD"].ToString()),
                        Token = msg["Token"].ToString(),
                        //AccessLevel = msg.Get<int>("alv"),
                        //IsOverLimit = msg.Get<bool>("isOverMoneyLimit")
                    }, isStatic: true);

                    List<IDictionary<string, object>> gameSessions = DynamicParser.ToDynamicList(msg["Games"]);
                    Dictionary<string, object> tmp = gameSessions.Select((d, i) => new {
                        GID = d["GID"].ToString(),
                        Index = i,
                        Value = d
                    })
                        .ToDictionary(d => d.GID, d => new GameSession() {
                            Number = d.Index + 1,
                            GID = d.Value["GID"].ToString()
                        } as object);
                    DataShared.Instance.SetData("GameSessions", tmp);
                    UIManager.Instance.TogglePanel("Login", false);

                    GameObject pGame = UIManager.Instance.TogglePanel("JJJView", true);
                    PanelGame pg = pGame.GetComponent<PanelGame>();
                    pg.Initialize();

                    if (tmp.Count > 0) {
                        this.Send("enter", new {
                            GID = tmp.Keys.First()
                        });
                    }
                });
            RegistryHandler("double login",
                Receive: msg => {
                    Disconnect();
                    UIManager.Instance.TogglePanel("Login", true);
                });
        }

        public void Awake() {
            DataShared.Instance.SetGame(GID);
            DataShared.Instance.SetData(HandlerID, this);
            IsAutoReconnectEnabled = true;
        }

        public void Start() {
            //PanelList.Foreach(p => {
            //    UIManager.Instance.PanelList.TryAdd(p.name, p);
            //    p.SetActive(false);
            //});

            UIManager.Instance.CanvasRoot = GameObject.Find("MainCanvas");

            //UIManager.Instance.TogglePanel("Loading", true);

            //connection.OnStateChange = s => {
            //    UIManager.Instance.PanelList["Loading"].GetComponent<LoadingScene>().Text = "Connection " + s.ToString();
            //};

            gameObject.scene.GetRootGameObjects().Foreach(DefaultResManager.SetFont);
        }

        public override void Connect() {
            base.Connect();
            if (connection.state == Connection.States.Connecting) {
                StartConnecting();
            }
        }

        public override void onOpen() {
            base.onOpen();
            string token = DataShared.Instance.GetData<string>(UserTokenKey);
            if (!string.IsNullOrEmpty(token)) {
                //Send("login", new {
                //    autcode = token
                //});
            } else {
                UIManager.Instance.TogglePanel("Login", true);
                //UIManager.Instance.TogglePanel("Loading", false);
                EndConnecting();
                //ToggleLoading(false);
            }
        }

        public override void onError(string errorMsg = "") {
            base.onError(errorMsg);
        }

        public override void onClose() {
            base.onClose();

            //ToggleLoading(true);
        }

        public void ToggleLoading(bool on, float progress = 0) {
            return;
            var Loading = UIManager.Instance.PanelList["Loading"].GetComponent<LoadingScene>();
            UIManager.Instance.TogglePanel("Loading", true);
            Loading.progress = progress;
            Loading.gameObject.SetActive(on);
        }

        public override void OnStartConnecting() {
            UIManager.Instance.TogglePanel("Connecting", true);
        }

        public override void OnEndConnecting() {
            UIManager.Instance.TogglePanel("Connecting", false);
        }
    }
}