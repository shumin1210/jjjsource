﻿using System.Collections;
using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace JJJ {

    public class ResultView : MonoBehaviour {
        public static string ResResult = "result";

        #region CenterView

        [SerializeField]
        private Text txtWin;

        [SerializeField]
        private Image imgResultSingle;

        [SerializeField]
        private Image imgResultDouble;

        [SerializeField]
        private Image imgPairSingle;

        [SerializeField]
        private Image[] imgPairDouble;

        [SerializeField]
        private AudioQueue AudioPlayer;

        #endregion CenterView

        #region InfoView

        [SerializeField]
        private GameObject ResultInfo;

        [SerializeField]
        private Text TxtInfo;

        [SerializeField]
        private Text TxtPlayer;

        [SerializeField]
        private Text TxtBanker;

        [SerializeField]
        private GameObject[] Pokers;

        #endregion InfoView

        private string[] Audios;

        public ResultView() {
           
        }

        private ResultData result;
        private int win;

        private void OnEnable() {
            ResultInfo.SetActive(true);
        }

        private void OnDisable() {
            ResultInfo.SetActive(false);
        }

        private string pattern = "{0}廳:{1} ";

        public void SetData(GameSession gs) {
            result = gs.Result;
            if (result.WinTypes == null) {
                return;
            }

            string info = pattern.format(gs.Number, result.Round);
            List<string> winSections = new List<string>();
            
            long totalWin = result.Wins.Sum();
            winSections.Add("贏分:" + totalWin);
            txtWin.text = totalWin.ToString();
            TxtInfo.text = info + winSections.Join(" ");

            TxtPlayer.text = "閒" + result.Points[0];
            TxtBanker.text = "莊" + result.Points[1];
            
            update();
        }

        public void Play(GameSession gs, bool playAudio) {
            SetData(gs);
            ResultInfo.SetActive(true);

            if (playAudio) {
                Audios = new string[5] {
                    "player",
                    "p{0}point".format(result.Points[0]),
                    "banker",
                    "p{0}point".format(result.Points[1]),
                    "w" + win,
                };
                AudioPlayer.Play(DataShared.Instance.Audios.Load(Audios));
            } else {
                Debug.Log("[ResultView] skip audio by insufficient time.");
            }
        }

        private void update() {
            if (result.WinTypes == null) {
                return;
            }
            win = result.WinTypes.ToList().IndexOf(true);
            if (result.WinTypes[2]) {
                win = 2;
            }

            int[] pairs = result.WinTypes.FindIndexes(v => v).Where(v => v > 2 && v < 5).ToArray();

            switch (pairs.Count()) {
                case 0:
                    imgResultSingle.gameObject.SetActive(true);
                    imgResultSingle.sprite = DataShared.Instance.Sprites.GetAtlas(ResResult)["r" + win];

                    imgResultDouble.gameObject.SetActive(false);
                    imgPairSingle.gameObject.SetActive(false);
                    imgPairDouble.Foreach(p => p.gameObject.SetActive(false));
                    break;

                case 1:
                    imgResultDouble.gameObject.SetActive(true);
                    imgResultDouble.sprite = DataShared.Instance.Sprites.GetAtlas(ResResult)["r" + win];

                    imgPairSingle.gameObject.SetActive(true);
                    imgPairSingle.sprite = DataShared.Instance.Sprites.GetAtlas(ResResult)["r" + pairs[0]];

                    imgResultSingle.gameObject.SetActive(false);
                    imgPairDouble.Foreach(p => p.gameObject.SetActive(false));
                    break;

                case 2:
                    imgResultDouble.gameObject.SetActive(true);
                    imgResultDouble.sprite = DataShared.Instance.Sprites.GetAtlas(ResResult)["r" + win];

                    imgPairDouble.Foreach((p, i) => {
                        p.sprite = DataShared.Instance.Sprites.GetAtlas(ResResult)["r" + pairs[i]];
                        p.gameObject.SetActive(true);
                    });

                    imgResultSingle.gameObject.SetActive(false);
                    imgPairSingle.gameObject.SetActive(false);
                    break;
            }
        }
    }
}