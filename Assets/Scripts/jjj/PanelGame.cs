﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Core;
using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;

namespace JJJ {

    /// <summary>遊戲畫面核心</summary
    public class PanelGame : MonoBehaviour {
        
        /// <summary>通訊實體</summary>
        private JJJHandler handler;

        /// <summary>單畫面的GID</summary>
        private string GIDofSingleGame;

        #region GameObjects
        
        [SerializeField]
        private GameLobby GameLobby;

        [SerializeField]
        private SingleGame SingleGamePanel;
        
        #endregion GameObjects

        private void Awake() {
            
        }

        private void Start() {
            handler = DataShared.Instance.GetData<JJJHandler>(JJJHandler.HandlerID);
            if (handler == null) {
                return;
            }
            registryHandlers();
        }

        public void Initialize() {
            IDictionary<string, object> games = DataShared.Instance.GetData<IDictionary<string, object>>("GameSessions");
            if (games == null) {
                return;
            }
            gameObject.SetActive(true);
            //SingleGamePanel.ChangeTable(true);
            //PanelLobby.SwitchGameSession(games.Keys.ElementAt(0));

        }
        
        #region 指令處理

        /// <summary>註冊指令</summary>
        private void registryHandlers() {
            handler.RegistryHandler("enter", Receive: ReceiveEnter);
            handler.RegistryHandler("leave", Receive: ReceiveLeave);
            handler.RegistryHandler("status", Receive: ReceiveStatus);
            handler.RegistryHandler("gamedata", Receive: ReceiveGameData);
                        
            handler.RegistryHandler("playerinfo", Receive: ReceivePlayerInfo);
            handler.RegistryHandler("update.playercount", Receive: ReceivePlayerCount);
            handler.RegistryHandler("bet", Receive: ReceiveBet);
           // handler.RegistryHandler("error", Receive: ReceiveError);

            //handler.RegistryHandler("serve", Send: () => new GameLibrary.Net.ActionObject("serve"));
        }

        /// <summary>處理指令:錯誤</summary>
        /// <param name="obj">訊息內容</param>
        private void ReceiveError(IDictionary<string, object> obj) {
            string gid = "";
            if (obj.TryGet("GID", ref gid)) {
                GameSession gs = getGameSessionByGID(gid);
                if (obj.ContainsKey("Msg")) {
                    switch (obj.Get("Msg")) {
                        case "餘額不足":
                            getGameScreen(gid).Audio.PlayClip("obaca/audio/OverCredit");
                            break;

                        case "超出下注上限":
                            getGameScreen(gid).Audio.PlayClip("obaca/audio/OverLimit");
                            break;
                    }
                }
            }
        }

        /// <summary>發送指令:請求遊戲紀錄</summary>
        public void RequestRecord() {
            if (!string.IsNullOrEmpty(GIDofSingleGame)) {
                handler.StartConnecting("request.record");
                handler.Send("request.record", new {
                    GID = GIDofSingleGame
                });
            }
        }

        /// <summary>接收指令:遊戲紀錄</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveRecord(IDictionary<string, object> msg) {
            TDataTable dataTable = new TDataTable();
            string[] columns = DynamicParser.ToArray(msg["Column"]);
            object[][] rows = (msg["Record"] as IList<object>).Select(o => (o as IList<object>).ToArray()).ToArray();
            dataTable.Load(columns, rows);

            SingleGamePanel.SetRecordData(dataTable);
        }

        private void ReceivePlayerCount(IDictionary<string,object> msg) {            
            GameSession gs = getGameSessionByGID(msg.Get("GID"));

            int viewerCount = msg.Get<int>("ViewerCount");
            int orderCount= msg.Get<int>("OrderCount");

            gs.ViewerCount = viewerCount;
            gs.OrderCount = orderCount;

            IGameScreen screen = getGameScreen(gs.GID);
            screen.RequestUpdate();
        }

        /// <summary>接收指令:遊戲資料</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveGameData(IDictionary<string, object> msg) {
            GameSession gs = getGameSessionByGID(msg.Get("GID"));
            //msg.TryGet("IsMaintenance", ref gs.IsMaintenance);

            //if (msg.TryGet("Round", ref gs.Round)) {
            //    gs.Limit = msg.Get<long>("Limit");
            //    gs.EndTime = ServerTime.ConvertFromServerDateString(msg.Get("Time_End"));
                
                gs.State = (GameState)msg.Get<int>("Status");
            gs.isPlayering = msg.Get<bool>("NowPlaying");
            //    gs.DurationCheck_ms = msg.Get<int>("CheckTime") * 1000;
            //}

            IGameScreen screen = getGameScreen(gs.GID);
            if (screen != null) {
                screen.RequestUpdate();
            } else {
                //GameLobby.Refresh();
            }
        }
        
        /// <summary>接收指令:結算資料</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveResult(IDictionary<string, object> msg) {
            Player player = DataShared.Instance.GetData<Player>(KEYS.USER, true);
            msg.TryGet<long>("Refund", ref player.Refund);
            msg.TryGet<long>("UD", ref player.Credit);

            GameSession gs = getGameSessionByGID(msg.Get("GID"));
            if (msg.ContainsKey("Bet")) {
                //gs.PlayerBet = DynamicParser.ToArray<long>(msg["Bet"]);
            }

            long[] wins = msg.ContainsKey("Win") ? DynamicParser.ToArray<long>(msg["Win"]) : new long[6];

            gs.Result = new ResultData {
                //Points = gs.Points,
                Round = msg.Get("Round"),
                Wins = wins,
                WinTypes = DynamicParser.ToArray<int>(msg["WinTypes"]).Cast(p => p == 1)
            };            
        }

        /// <summary>接收指令:進入遊戲</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveEnter(IDictionary<string, object> msg) {
            /*player info*/
            Player user = DataShared.Instance.GetData<Player>(KEYS.USER, true);
            //user.Refund = msg.Get<long>("Refund");
            //user.Credit = msg.Get<long>("UD");

            /*gamesession info*/
            GameSession gs = getGameSessionByGID(msg.Get("GID"));
            gs.Seat = 1;

            GIDofSingleGame = gs.GID;
            gs.VideoURL = msg.Get("VideoURL");

            SingleGamePanel.SetGameSession(gs);
            //gs.Result = ResultData.DefaultValue;
            //gs.occupied = true;
            //gs.Seat = msg.Get<int>("Seat");

            //gs.Limit = msg.Get<long>("Limit");

            //gs.State = (GameState)msg.Get<int>("Status");

            
            //gs.DurationCheck_ms = msg.Get<int>("CheckTime") * 1000;
            //gs.EndTime = ServerTime.ConvertFromServerDateString(msg.Get("Time_End"));

        }

        /// <summary>接收指令:離開遊戲</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveLeave(IDictionary<string, object> msg) {
            string gid = msg["GID"].ToString();
            if (gid == GIDofSingleGame) {
                GIDofSingleGame = "";
                SingleGamePanel.ClearGameSession();
                getGameSessionByGID(gid).occupied = false;
            }
        }

        /// <summary>接收指令:遊戲狀態變更</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveStatus(IDictionary<string, object> msg) {
            GameSession gs = getGameSessionByGID(msg["GID"].ToString());
            gs.EndTime = ServerTime.ConvertFromServerDateString(msg["Time_End"].ToString());
            GameState state = (GameState)Convert.ToInt32(msg["Status"]);
            bool isChange = gs.State != state;
            gs.State = state;
            getGameScreen(gs.GID).RequestUpdate(true);
            switch (gs.State) {
                case GameState.Ready:
                    gs.Result = ResultData.DefaultValue;
                    break;

                //case GameState.Bet:
                //    getGameScreen(gs.GID).Audio.PlayClip("obaca/audio/startbet");
                //    break;
                
                case GameState.Restart:
                    gs.Restart();
                    break;
                    
            }
        }
        
        /// <summary>接收指令:押注資料更新</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveUpdateBet(IDictionary<string, object> msg) {
            GameSession gs = getGameSessionByGID(msg["GID"].ToString());
            
            getGameScreen(gs.GID).RequestUpdate();
        }

        /// <summary>接收指令:玩家資料更新</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceivePlayerInfo(IDictionary<string, object> msg) {
            Player pl = DataShared.Instance.GetData<Player>(KEYS.USER, true);
            msg.TryGet<long>("Refund", ref pl.Refund);
            msg.TryGet<long>("UD", ref pl.Credit);
            msg.TryGet<bool>("IsOverMoneyLimit", ref pl.IsOverLimit);

            if (msg.ContainsKey("GID")) {
                GameSession gs = getGameSessionByGID(msg.Get("GID"));
                
                getGameScreen(gs.GID).RequestUpdate();
            }
        }

        /// <summary>接收指令:下注成功</summary>
        /// <param name="msg">訊息內容</param>
        private void ReceiveBet(IDictionary<string, object> msg) {
            //Player pl = DataShared.Instance.GetData<Player>(KEYS.USER, true);
            //pl.Refund = msg.Get<long>("Refund");
            //pl.Credit = msg.Get<long>("UD");

            //GameSession gs = getGameSessionByGID(msg["GID"].ToString());
            //gs.BetSum = DynamicParser.ToArray<Int64>(msg["AllBets"]);
            //gs.PlayerBet = DynamicParser.ToArray<Int64>(msg["Bets"]);

            //BetZone zone = (BetZone)Convert.ToInt32(msg["zone"]);
            //long amount = Convert.ToInt64(msg["amount"]);
            //gs.PlayerBetedChips[(int)zone].Add(amount);
            
            //getGameScreen(gs.GID).RequestUpdate();
        }
        
        #endregion 指令處理

        /// <summary>取得對應GID的遊戲畫面物件</summary>
        /// <param name="gid">指定GID</param>
        /// <returns>遊戲畫面物件</returns>
        private IGameScreen getGameScreen(string gid) {            
            return SingleGamePanel.GetComponent<IGameScreen>();            
        }

        /// <summary>以GID取得GameSession</summary>
        /// <param name="gid">GID</param>
        /// <returns>返回指定GID的GameSession</returns>
        private GameSession getGameSessionByGID(string gid) {
            return DataShared.Instance.GetData<GameSession>("GameSessions." + gid);
        }
                
        /// <summary>畫面更新:呼叫[修改密碼]UI</summary>
        public void EditPassword() {
            UIManager.Instance.TogglePanel("PanelEditPwd", true);
        }

        /// <summary>指令:登出</summary>
        public void Logout() {
            handler.Disconnect();
        }
    }
}