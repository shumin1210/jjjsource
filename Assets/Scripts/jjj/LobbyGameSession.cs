﻿using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace JJJ {

    public class LobbyGameSession : MonoBehaviour {

        [SerializeField]
        private Image m_img;

        [SerializeField]
        private LocalizedText text;

        private Sprite[] images = new Sprite[2];
        public GameLobby parent;

        public string GID;
        public int No;
        public bool isSelected;

        private void Start() {
            SetText();
        }

        public void SetText() {
            text.SetArgs(No);
        }

        public void SetSelected(bool val) {
            isSelected = val;
            m_img.sprite = DataShared.Instance.Sprites.GetAtlas(RES.LOBBY)["session" + (isSelected ? 1 : 0)];
        }

        public void SetGameSessionInfo() {
            parent.SwitchGameSession(GID);
        }
    }
}