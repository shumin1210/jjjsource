﻿using GameLibrary.Utility;
using UnityEngine;

namespace JJJ {
    public enum CommandBtnKey {
        Grab,
        Left,
        Right
    }

    public enum GameState {
        Ready = 0,
        Wait = 1,
        WAITTOPLAY = 2,
        Play = 3,
        Restart = 4,
        Maintenance = 5
    }
        
    public struct ResultData {
        public string Round;
        public bool[] WinTypes;
        public long[] Wins;
        public int[] Points;
        public static readonly ResultData DefaultValue = default(ResultData);
    }
    
    public struct COLOR {
        public const string TEXT_PATTERN = "<color={1}>{0}</color>";
        public const string PLAYER = "#1E90FF";
        public const string BANKER = "#FF0000";
        public const string TIE = "#008000";
    }

    public struct RES {
        public const string LOBBY = "ui_lobby";
        public const string RECORD = "record";
        public const string STATES = "states";
    }

    public struct KEYS {
        public const string USER = "User";
        public const string CACHE = "player_cache_login";
        public const string ACCOUNT = "player_account";
        public const string PASSWORD = "player_password";
    }
    
    public struct Command {
        public const string Up = "W";
        public const string Down = "S";
        public const string Left="A";        
        public const string Right = "D";
        
        public const string Grab = "J";
        public const string None = "";
    }
}