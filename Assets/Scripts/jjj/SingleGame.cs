﻿using System.Collections.Generic;
using System.Linq;
using GameLibrary.UI;
using UnityEngine;
using UnityEngine.UI;
using GameLibrary.Utility;
using System;

namespace JJJ {

    public class SingleGame : IGameScreen {

        #region GameObjects
        
        [SerializeField]
        protected GameObject BtnRecord;
        
        #endregion GameObjects
        
        protected override void Start() {
            base.Start();            
        }

        public override void SetGameSession(GameSession pgs) {
            base.SetGameSession(pgs);            
        }

        #region Updates
        
        protected override void updateUI() {
            base.updateUI();

            bool hasBeted;
            switch (GameSession.State) {
                default:
                    //BtnRecord.SetActive(true);
                    break;

                //case GameState.Bet:
                //    hasBeted = GameSession.PlayerBet.Sum() > 0;
                //    BtnRecord.SetActive(!hasBeted);
                //    break;
                
            }
        }

        #endregion Updates

        public void SetRecordData(TDataTable data) {
            data.EditColumn("fee", dr => TConvert.ToBoolean(dr["fee"].ToString().Split(",")[0]));
            data.EditColumn("pokers", dr => dr["pokers"].ToString().Split(",").Select(d => Convert.ToInt32(d)).ToArray());
            data.EditColumn("points", dr => dr["points"].ToString().Split(":").Select(d => Convert.ToInt32(d)).ToArray());

            data.AddColumn("winner", dr => {
                int[] pokers = dr.Get<int[]>("pokers");
                if (pokers[0] == 0) {
                    return COLOR.TEXT_PATTERN.format("error", "red");
                }

                int[] points = dr.Get<int[]>("points");
                if (points[0] == points[1]) {
                    return COLOR.TEXT_PATTERN.format("和", COLOR.TIE);
                } else if (points[0] < points[1]) {
                    return COLOR.TEXT_PATTERN.format("莊贏", COLOR.BANKER);
                } else {
                    return COLOR.TEXT_PATTERN.format("閒贏", COLOR.PLAYER);
                }
            });            
        }

        #region ButtonClickEvents

        public void OpenRecordView() {
            panelGame.RequestRecord();
        }
        
        #endregion ButtonClickEvents
    }
}