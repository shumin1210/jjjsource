﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameLibrary.Core;
using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace JJJ {

    /// <summary>遊戲畫面</summary>
    public abstract class IGameScreen : MonoBehaviour {
        public PanelGame panelGame;
        public GameLobby GameLobby;
        protected GameState preState = GameState.Ready;

        protected bool _enabledVideo = false;

        protected bool EnableVideo {
            get {
                return HasGame && _enabledVideo;
            }
            set {
                _enabledVideo = value;
                //if (HasGame) {
                //    GameSession.VideoDisplay = value ? VideoDisplay.Camera2 : VideoDisplay.Camera1;
                //}
            }
        }

        #region Fields

        protected string GID;

        public GameSession GameSession {
            get;
            protected set;
        }

        public bool IsDisplayed {
            get;
            protected set;
        }

        public bool HasGame {
            get {
                return GameSession != null;
            }
        }

        protected bool isDoubleScreen = false;
        public bool holdForChangeTable = false;
        protected Player user;
        protected JJJHandler handler;
        protected bool needUpdate = true;

        protected int CountdownSec;
        protected int[] BetCountdownAlertTiming = new int[] { 10, 5 };

        /// <summary>使用中的籌碼的index</summary>
        protected int selectedChipIndex = 0;

        /// <summary>已顯示的押注籌碼,對應GameSession.PlayerBetedChips各區押注籌碼數量,的索引值</summary>
        protected List<ChipBeted>[] playerBets = new List<ChipBeted>[6];

        #endregion Fields

        #region GameObjects

        [SerializeField]
        protected UltraVideoPlayer VideoPlayer;

        [SerializeField]
        protected GameObject Video;
        
        [SerializeField]
        public AudioSource Audio;

        [SerializeField]
        protected AudioClip SEAlert;
        
        #region 基本資訊
                
        [SerializeField]
        protected Text TextViewerCount;

        [SerializeField]
        protected Text TextOrderCount;

        #endregion 基本資訊

        #region 按鈕

        [SerializeField]
        protected Button BtnLeft;

        [SerializeField]
        protected Button BtnRight;

        [SerializeField]
        protected Button BtnChangeView;

        [SerializeField]
        protected Button BtnOrder;

        [SerializeField]
        protected Button BtnGrab;
        
        #endregion 按鈕

        #endregion GameObjects

        protected virtual void Start() {
        }

        protected virtual void OnEnable() {
            Audio.volume = 1f;
            if (EnableVideo) {
                VideoPlayer.Play();
            }
            updateAll();

            updateGameControl(false);
        }

        protected virtual void OnDisable() {
            Audio.volume = 0f;
            if (EnableVideo) {
                VideoPlayer.Pause();
            }
            updateGameControl(false);
        }

        public IGameScreen() {
            for (int i = 0; i < playerBets.Length; i++) {
                playerBets[i] = new List<ChipBeted>();
            }
        }

        public void SetActive(bool isActive) {
            if (isActive) {
                gameObject.SetActive(!holdForChangeTable);
                //GameLobby.gameObject.SetActive(holdForChangeTable);
            } else {
                gameObject.SetActive(false);
                //GameLobby.gameObject.SetActive(false);
            }
        }

        public void ClearGameSession() {
            GameSession = null;
        }

        /// <summary>設定此單廳畫面繫結的遊戲廳,並會對畫面上的資訊做更新</summary>
        /// <param name="pgs">遊戲廳物件</param>
        public virtual void SetGameSession(GameSession pgs) {
            user = DataShared.Instance.GetData<Player>(KEYS.USER, true);
            handler = DataShared.Instance.GetData<JJJHandler>(JJJHandler.HandlerID);
            GameSession = pgs;
            if (!HasGame) {
                ClearGameSession();
                return;
            }

            holdForChangeTable = false;
            SetActive(true);

            string videoPath = pgs.VideoURL;
               //videoPath2 = "rtmp://rtmp.7sbc888.com/live2/stream";
            if (videoPath != VideoPlayer.Path) {
                VideoPlayer.Stop();
                VideoPlayer.Path = videoPath;
            }
            if (!VideoPlayer.IsPlaying) {
                VideoPlayer.Play();
            }
            //if (videoPath2 != VideoPlayer2.Path) {
            //    VideoPlayer2.Stop();
            //    VideoPlayer2.Path = videoPath2;
            //}
            //if (!VideoPlayer2.IsPlaying) {
            //    VideoPlayer2.Play();
            //}

            reset();
        }

        protected virtual void reset() {
            RequestUpdate();
        }

        private void Update() {
            if (!HasGame) {
                return;
            }
            if (needUpdate) {
                updateAll();
                needUpdate = false;
            } else {
                updateDynamics();
            }
        }

        #region Updates

        public void RequestUpdate(bool force = false) {
            if (force) {
                updateAll();
            } else {
                needUpdate = true;
            }
        }

        private void updateDynamics() {
            updateState();
            updateUI();
        }

        private void updateAll() {
            if (GameSession == null) {
                ChangeTable();
                return;
            }

            updateCount();
            updatePlayerInfo();
            updateGameInfo();
            updateState();
            updateUI();
        }

        protected virtual void updateUI() {
            if (preState != GameSession.State) {
                preState = GameSession.State;
                switch (GameSession.State) {
                    case GameState.Ready:
                        break;
                        
                    case GameState.Maintenance:
                        break;
                }
            } else {
            }
            switch (GameSession.State) {
                case GameState.Ready:
                case GameState.Wait:
                    BtnOrder.gameObject.SetActive(true);
                    updateGameControl(false);
                    break;
                case GameState.Play:
                    BtnOrder.gameObject.SetActive(!GameSession.isPlayering);
                    updateGameControl(GameSession.isPlayering);                    
                    break;
                case GameState.Maintenance:
                case GameState.Restart:
                    //BtnChange.SetActive(true);
                    //BtnRefund.SetActive(GameSession.EnableCloseRefund);
                    
                    //BtnVideo.SetActive(false);
                    break;
                    
            }
            refreshVideo();
        }

        protected void updateCount() {
            TextViewerCount.text = GameSession.ViewerCount.ToString();
            TextOrderCount.text = GameSession.OrderCount.ToString();
        }

        protected virtual void refreshVideo() {
            if (!HasGame) {
                //Video.SetActive(false);
                return;
            }
            //if (GameSession.State == GameState.Result) {
            //    EnableVideo = false;
            //}
            bool enableVideo = EnableVideo = true;
            //if (GameSession.State != GameState.Shuffle) {
            //    enableVideo = enableVideo && GameSession.PlayerBet.Sum() > 0;
            //}

            if (enableVideo) {
                if (!VideoPlayer.IsPlaying) {
                    VideoPlayer.Play();
                }
            } else {
                if (VideoPlayer.IsPlaying) {
                    VideoPlayer.Pause();
                }
            }
            if (enableVideo) {                
                VideoPlayer.Toggle(true);  
            } else {
                VideoPlayer.Toggle(enableVideo);
            }
            
        }

        protected virtual void updateState() {
            
        }

        protected void updateGameControl(bool enabled) {
            BtnLeft.enabled = enabled;
            BtnChangeView.enabled = enabled;
            BtnRight.enabled = enabled;
            BtnGrab.enabled = enabled;

            BtnGrab.gameObject.SetActive(enabled);            
        }

        protected virtual void updatePlayerInfo() {
            return;
            //TextPlayerName.text = user.Name;
            
        }

        protected virtual void updateGameInfo() {
            Player player = DataShared.Instance.GetData<Player>(KEYS.USER, isStatic: true);            
        }
               

        #endregion Updates
        
        #region GameObject Functions

        /// <summary>下注動作</summary>
        /// <param name="zone">指定下注區</param>
        public void BetIn(string zone) {
            //BetZone betZone = (BetZone)Enum.Parse(typeof(BetZone), zone);
            //long amount = getBetAmount();
            //int i = (int)betZone;
            //if (GameSession.State != GameState.Bet && GameSession.State != GameState.Ready) {
            //    return;
            //}
            //if (GameSession.PlayerBet[i] + amount > GameSession.Max[i]) {
            //    Audio.PlayClip("obaca/audio/OverLimit");
            //    return;
            //}
            //if (betZone == BetZone.Player && GameSession.PlayerBet[(int)BetZone.Banker] > 0) {
            //    return;
            //}
            //if (betZone == BetZone.Banker && GameSession.PlayerBet[(int)BetZone.Player] > 0) {
            //    return;
            //}
            //if (betZone == BetZone.BankerSix && GameSession.FeeType) {
            //    return;
            //}

            //handler.Send("bet", new {
            //    GID = GameSession.GID,
            //    zone = (int)betZone,
            //    amount
            //});
        }

        public void CloseVideo() {
            EnableVideo = false;
            refreshVideo();
        }

        public void ShowCheck() {
            VideoPlayer.Pause();
            toggleCheckAndVideo(true);
        }

        /// <summary>顯示視訊</summary>
        public void ShowVideo() {
            toggleCheckAndVideo(false);
            VideoPlayer.Play();
        }

        private void toggleCheckAndVideo(bool check) {
            EnableVideo = !check;
            RequestUpdate();
        }
        
        public void ChangeTable() {
            //ChangeTable(false);
        }

        /// <summary>換桌</summary>
        public void ChangeTable(bool justShowLobby) {
            holdForChangeTable = true;
            if (!justShowLobby && GameSession != null) {
                GameLobby.ChangeGID = GameSession.GID;
                GameLobby.GID = GameSession.GID;
            }
            gameObject.SetActive(false);
            GameLobby.SetActive(true);
        }

        /// <summary>請求結算退水</summary>
        public void RequestRefund() {
            handler.Send("request.closerefund", new {
                GID = GameSession.GID
            });
        }
        
        /// <summary>往左</summary>
        public void ClickLeft() {
            handler.Send("command", new {
                GameSession.GID,
                key = GameSession.GetCommandKey(CommandBtnKey.Left)
            });
        }

        /// <summary>往右</summary>
        public void ClickRight() {
            handler.Send("command", new {
                GameSession.GID,
                key = GameSession.GetCommandKey(CommandBtnKey.Right)
            });
        }

        /// <summary>夾</summary>
        public void ClickGrab() {
            handler.Send("command", new {
                GameSession.GID,
                key = GameSession.GetCommandKey(CommandBtnKey.Grab)
            });
        }

        /// <summary>改變視角</summary>
        public void ChangeView() {
            
            refreshVideo();
        }

        public void Order() {
            handler.Send("order", new {
                GameSession.GID
            });
        }
        
        #endregion GameObject Functions
    }
}