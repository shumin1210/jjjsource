﻿using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace JJJ {

    public class PanelEditPwd : MonoBehaviour {
        private JJJHandler handler;

        [SerializeField]
        private InputField TxtPassword_old;

        [SerializeField]
        private InputField TxtPassword_new;

        [SerializeField]
        private InputField TxtPassword_confirm;

        [SerializeField]
        private Text TxtMsg;

        private void Start() {
            handler = DataShared.Instance.GetData<JJJHandler>(JJJHandler.HandlerID);
            handler.RegistryHandler("edit.password",
                Receive: msg => {
                    if (msg.ContainsKey("Result")) {
                        TxtMsg.text = msg["Result"].ToString();
                    } else {
                        TxtMsg.text = "";
                    }
                });
        }

        public void ClosePanel() {
            UIManager.Instance.TogglePanel("PanelEditPwd", false);
        }

        public void SendEditPwd() {
            if (TxtPassword_confirm.text == TxtPassword_new.text) {
                handler.Send("edit.password", new {
                    oldpwd = TxtPassword_old.text,
                    newpwd = TxtPassword_new.text
                });
                TxtMsg.text = "";
            } else {
                TxtMsg.text = "確認密碼錯誤";
            }
        }
    }
}