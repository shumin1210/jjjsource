﻿using System;
using System.Runtime.InteropServices;
using GameLibrary.UI;
using GameLibrary.Utility;
using UnityEngine;

public class UltraVideoPlayer : UniversalMediaPlayer {
#if UNITY_WEBGL

    private string Name {
        get {
            return gameObject.name;
        }
    }

    private bool init = false;

    [NonSerialized]
    public new bool IsPlaying = false;

    private bool isDisplayed = false;

    [DllImport("__Internal")]
    public static extern void PlayVideo(string id, string src);

    [DllImport("__Internal")]
    public static extern void ToggleVideoPlayer(string id, int on);

    [DllImport("__Internal")]
    public static extern void PauseVideoPlayer(string id);

    [DllImport("__Internal")]
    public static extern void ResumeVideoPlayer(string id);

    [DllImport("__Internal")]
    public static extern void SetVideoPlayerRect(string id, float x, float y, float w, float h);

    public new void Play() {
        if (!init) {
            PlayVideo(Name, Path);
        } else if (!IsPlaying) {
            ResumeVideoPlayer(Name);
        }
    }

    public new void Pause() {
        if (IsPlaying) {
            PauseVideoPlayer(Name);
        }
    }

    public void Toggle(int on) {
        Toggle(on == 1);
    }

    public void Toggle(bool on) {
        RenderingObjects.Foreach(o => o.SetActive(false));
        if (on != isDisplayed) {
            isDisplayed = on;
            ToggleVideoPlayer(Name, isDisplayed ? 1 : 0);
            if (isDisplayed) {
                RefreshRect();
            }
        }
    }

    public void PlayStateChange(int isPlaying) {
        IsPlaying = isPlaying == 1;
    }

    public void RefreshRect() {
        if (RenderingObjects.Length == 0) {
            return;
        }
        Debug.Log("VideoPlayer RefreshRect");
        Rect rect = RectTransformExtensions.RectTransformToWebSpace(RenderingObjects[0].transform as RectTransform);
        SetVideoPlayerRect(Name, rect.x, rect.y, rect.width, rect.height);
    }

    private new void OnDisable() {
        base.OnDisable();
        if (init) {
            Toggle(false);
        }
    }

    private void OnEnable() {
        if (init && isDisplayed) {
            Toggle(true);
        }
    }

#else

    public void Toggle(bool on) {
        if (RenderingObjects != null) {
            RenderingObjects.Foreach(o => o.SetActive(on));
        }
    }

#endif
}