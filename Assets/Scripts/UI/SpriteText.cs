﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteText : MonoBehaviour {

    [SerializeField]
    private List<string> Sprites;

    [SerializeField]
    private float Duration;

    private float passedTime = 0;

    [SerializeField]
    private bool isActived;

    private void Update() {
        if (!isActived) {
            return;
        }
        Text txt = GetComponent<Text>();
        if (txt == null) {
            return;
        }
        string str = updateText();
        if (!string.IsNullOrEmpty(str)) {
            txt.text = str;
        }
    }

    public void StartAnim() {
        passedTime = 0;
        isActived = true;
    }

    private string updateText() {
        if (Sprites.Count == 0) {
            return "";
        }

        if (!isActived) {
            return Sprites[0];
        } else {
            passedTime = (passedTime + Time.deltaTime) % Duration;
            return Sprites[(int)(Sprites.Count * passedTime / Duration)];
        }
    }
}