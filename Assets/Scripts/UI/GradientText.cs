﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[AddComponentMenu("UI/Effects/Gradient")]
public class GradientText : BaseMeshEffect {
    public Color32 TopLeftColor = Color.white;
    public Color32 TopRightColor = Color.white;
    public Color32 BottomRightColor = Color.black;
    public Color32 BottomLeftColor = Color.black;

    public override void ModifyMesh(VertexHelper vh) {
        if (!IsActive()) {
            return;
        }

        if (vh.currentVertCount == 0) {
            return;
        }

        List<UIVertex> list = new List<UIVertex>();
        vh.GetUIVertexStream(list);

        float bottom = list.Min(v => v.position.y);
        float top = list.Max(v => v.position.y);
        float right = list.Max(v => v.position.x);
        float left = list.Min(v => v.position.x);

        float uiElementWidth = right - left;
        float uiElementHeight = top - bottom;

        for (int i = 0; i < vh.currentVertCount; i++) {
            UIVertex v = new UIVertex();
            vh.PopulateUIVertex(ref v, i);
            float horizontalDelta = (v.position.x - left) / uiElementWidth;
            v.color = Color32.Lerp(Color32.Lerp(BottomLeftColor, BottomRightColor, horizontalDelta), Color32.Lerp(TopLeftColor, TopRightColor, horizontalDelta), (v.position.y - bottom) / uiElementHeight);
            vh.SetUIVertex(v, i);
        }
    }
}