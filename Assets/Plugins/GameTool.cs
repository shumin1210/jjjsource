﻿using System.Collections.Generic;
using GameDevWare.Serialization;

namespace GameLibrary.Utility {

    public static class JS {

        public static string Serialize(object o) {
            return Json.SerializeToString(o, SerializationOptions.SuppressTypeInformation);
        }

        public static IDictionary<string, object> Deserialize(string str) {
            return Json.Deserialize<IDictionary<string, object>>(str, SerializationOptions.SuppressTypeInformation);
        }

        public static T Deserialize<T>(string str) {
            return Json.Deserialize<T>(str, SerializationOptions.SuppressTypeInformation);
        }
    }
}