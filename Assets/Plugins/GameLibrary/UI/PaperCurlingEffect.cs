﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GameLibrary.Utility;

namespace GameLibrary.UI {

    /// <summary>紙張彎曲效果</summary>
    [ExecuteInEditMode]
    public class PaperCurlingEffect : MonoBehaviour {

        #region Fields

        #region GameObjects

        /// <summary>遮罩</summary>
        /// <remarks>pivot設定在切線中點,寬高最短為Front的對角線長度</remarks>
        [SerializeField]
        private RectTransform Mask;

        /// <summary>正面</summary>
        /// <remarks>Mask的子項,同時為此元件的綁定目標</remarks>
        [SerializeField]
        private RectTransform Front;

        /// <summary>背面</summary>
        /// <remarks>Front的子項</remarks>
        [SerializeField]
        private RectTransform Back;

        /// <summary>陰影</summary>
        /// <remarks>Back的子項,pivot定在Back的邊界,同時為Mask的邊界依據</remarks>
        [SerializeField]
        private RectTransform GradientShadow;

        #endregion GameObjects

        #region Publics

        [SerializeField]
        private Vector3 vector = Vector3.zero;

        /// <summary>代表力矩來源方向</summary>
        public Vector3 Vector {
            get {
                return vector;
            }
            set {
                needUpdate = true;
                vector = Sensitive * value;
            }
        }

        /// <summary>對力矩的敏感度</summary>
        public float Sensitive = .1f;

        /// <summary>最大彎折程度</summary>
        public float MaxCurl = .5f;

        /// <summary>水平方向調整參數</summary>
        public float XMulti = 1f;

        /// <summary>垂直方向調整參數</summary>
        public float YMulti = 1f;

        /// <summary>整體物件預設旋轉方向(未實作)</summary>
        public float BaseDegree = 0f;

        public bool ForceUpdate = false;

        #endregion Publics

        [SerializeField]
        private float degree = 0f;

        [SerializeField]
        private float distance;

        [SerializeField]
        private float folded = 0f;

        [SerializeField]
        private float diagnoDegree;

        [SerializeField]
        private Vector3 unit;

        [SerializeField]
        private bool needUpdate = false;

        public void SetZero() {
            Vector = Vector3.zero;
        }

        #endregion Fields

        private void LateUpdate() {
            //calc
            if (ForceUpdate || needUpdate) {
                needUpdate = false;
                calcDegree();
                calcDistance();
                setTransforms();
            }
        }

        private void setTransforms() {
            //set zero
            Vector3 pos = Front.position;
            Front.eulerAngles = Vector3.zero;

            //face
            Back.localPosition = unit * (1 - folded) * distance;
            Back.eulerAngles = new Vector3(0.0f, 0.0f, degree * 2);

            //shadow
            GradientShadow.position = (pos + Back.position) / 2;
            GradientShadow.eulerAngles = new Vector3(0.0f, 0.0f, degree + 180);

            //mask
            Mask.position = GradientShadow.position;
            Mask.eulerAngles = new Vector3(0.0f, 0.0f, degree + BaseDegree + 90);

            Front.position = pos;
            Front.eulerAngles = Vector3.zero;
            Front.Rotate(new Vector3(0, 0, BaseDegree));
        }

        private void calcDegree() {
            //get degree
            degree = Mathf.Atan2(Vector.y, Vector.x) * 180.0f / Mathf.PI;
            if (Vector.magnitude == 0) {
                unit = Vector3.right;
                degree = 0;
            } else {
                unit = Vector.normalized;
                while (degree < 0) {
                    degree += 360;
                }
            }
        }

        [SerializeField]
        private bool isVertical;

        private void calcDistance() {
            diagnoDegree = Mathf.Atan2(Front.rect.height, Front.rect.width) * 180.0f / Mathf.PI;
            isVertical = (degree > diagnoDegree && degree < 180 - diagnoDegree) || (degree > 180 + diagnoDegree && degree < 360 - diagnoDegree);
            distance = new Vector2(Back.rect.height * XMulti * Mathf.Cos(degree / 180.0f * Mathf.PI), Back.rect.width * YMulti * Mathf.Sin(degree / 180.0f * Mathf.PI)).magnitude;
            folded = Mathf.Min(Vector.magnitude / distance, MaxCurl);
        }
    }
}