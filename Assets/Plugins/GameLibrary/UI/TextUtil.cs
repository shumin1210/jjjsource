﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[ExecuteInEditMode]
public class TextUtil : MonoBehaviour {
    public string Format;

    public void SetValue(float args) {
        GetComponent<Text>().text = string.Format(Format, args);
    }

    public void SetValue(string args) {
        GetComponent<Text>().text = string.Format(Format, args);
    }

    public void SetValue(int args) {
        GetComponent<Text>().text = string.Format(Format, args);
    }
}