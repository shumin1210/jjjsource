﻿using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    public class ResizeHelper : MonoBehaviour {

        public enum ResizeType {
            Native, Origin, Image
        }

        public ResizeType type = ResizeType.Native;
        public RectTransform.Axis RefSide = RectTransform.Axis.Horizontal;
        private float width;
        private float height;
        private float ratio;

        private bool isChanged = false;

        private void Start() {
            ResetSize();
        }

        public void ResetSize() {
            RectTransform rect = transform as RectTransform;
            switch (type) {
                case ResizeType.Native:
                    break;

                case ResizeType.Origin:
                    width = rect.rect.width;
                    height = rect.rect.height;
                    ratio = width / height;
                    break;

                case ResizeType.Image:
                    width = rect.rect.width;
                    Image img = GetComponent<Image>();
                    if (img != null) {
                        height = img.sprite.rect.height * 1f / img.sprite.rect.width * width;
                    } else {
                        height = rect.rect.height;
                    }
                    ratio = width / height;
                    break;
            }
        }

        private void Update() {
            if (isChanged) {
                Resize();
                isChanged = false;
            }
        }

        private void OnRectTransformDimensionsChange() {
            isChanged = true;
        }

        public void Resize() {
            switch (type) {
                case ResizeType.Native:
                    resize_Native();
                    break;

                case ResizeType.Origin:
                    resize_Origin();
                    break;

                case ResizeType.Image:
                    resize_Origin();
                    break;
            }
        }

        private void resize_Native() {
        }

        private void resize_Origin() {
            RectTransform rect = transform as RectTransform;
            float nowRatio = rect.rect.width / rect.rect.height;
            if (nowRatio == ratio) {
                return;
            }
            float w, h;
            Vector2 stretchs = rect.anchorMax - rect.anchorMin;
            switch (RefSide) {
                case RectTransform.Axis.Horizontal:
                    w = rect.sizeDelta.x;
                    h = stretchs.y > 0 ? rect.sizeDelta.y : rect.rect.width / ratio;
                    rect.sizeDelta = new Vector2(w, h);
                    break;

                case RectTransform.Axis.Vertical:
                    w = stretchs.x > 0 ? rect.sizeDelta.x : rect.rect.height * ratio;
                    h = rect.sizeDelta.y;
                    rect.sizeDelta = new Vector2(w, h);
                    break;
            }
        }
    }
}