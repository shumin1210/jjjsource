﻿using GameLibrary.Utility;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.UI {

    /// <summary>多國語言核心</summary>
    public class LocalizationManager : Singleton<LocalizationManager> {

        /// <summary>語言包</summary>
        private struct LanguagePackage {
            public string language;
            public Dictionary<string, string> map;

            public string this[string key] {
                get {
                    return map.ContainsKey(key) ? map[key] : "";
                }
            }

            public LanguagePackage(string language, Dictionary<string, string> map) {
                this.language = language;
                this.map = map;
            }
        }

        /// <summary>語言映射</summary>
        private Dictionary<string, LanguagePackage> Map = new Dictionary<string, LanguagePackage>();

        /// <summary>使用中的語言ID</summary>
        private string language = "zh-tw";

        /// <summary>讀取語言包</summary>
        /// <param name="bundle">指定語言包資源的來源</param>
        private void loadLocalization(string bundle) {
            TextAsset txt = DataShared.Instance.TextAssets["localization"] ?? Resources.Load<TextAsset>(bundle + "/localization");

            if (txt == null) {
                return;
            }

            Dictionary<string, Dictionary<string, string>> dic = JS.Deserialize<Dictionary<string, Dictionary<string, string>>>(txt.text);
            if (dic == null
                || !dic.ContainsKey(language)) {
                return;
            }

            Dictionary<string, string> map = dic[language];
            if (!map.ContainsKey(bundle)) {
                Map.Add(bundle, new LanguagePackage(language, map));
            } else {
                Map[bundle] = new LanguagePackage(language, map);
            }
        }

        /// <summary>取得指定群組與key的語言字串</summary>
        /// <param name="bundle">群組ID</param>
        /// <param name="key">Key</param>
        /// <returns>只定語言的原始字串,若不包含此群組ID(bundle)或key值則回傳空字串</returns>
        public string GetLocalizedValue(string bundle, string key) {
            if (!Map.ContainsKey(bundle)
                || Map[bundle].language != language) {
                loadLocalization(bundle);
            }

            if (!Map.ContainsKey(bundle)) {
                return "";
            }

            return Map[bundle][key];
        }
    }
}