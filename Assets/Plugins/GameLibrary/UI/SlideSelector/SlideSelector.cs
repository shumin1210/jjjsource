﻿using System;
using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLibrary.UI {

    [ExecuteInEditMode]
    public class SlideSelector : MonoBehaviour {

        [Serializable]
        public class SelectionChangedEvent : UnityEvent<string> {
        }

        protected struct MovingData {
            public GameObject Target;
            public Vector3 From;
            public Vector3 To;
        }

        protected static string POSITION_INDEX = "slideSelPositionIndex";
        protected static string VALUE_ID = "slideSelItemValue";

        public GameObject SelectionWindow;

        public RectTransform OutterBegin;
        public RectTransform CenterPosition;
        public RectTransform OutterEnd;
        public GameObject PrefabSelectionItem;

        private RectTransform this[int index] {
            get {
                switch (index) {
                    default:
                    case 0:
                        return OutterBegin;

                    case 1:
                        return CenterPosition;

                    case 2:
                        return OutterEnd;
                }
            }
        }

        public int Count {
            get {
                return selections.Count;
            }
        }

        public bool IsActing {
            get; protected set;
        }

        public int SelectedIndex {
            get {
                return nowSelectedIndex;
            }
        }

        public string SelectedItem {
            get {
                return Count == 0 ? null : selections[nowSelectedIndex].Get(VALUE_ID);
            }
        }

        [SerializeField]
        internal int nowSelectedIndex = 0;

        public float Interval_sec = .5f;
        protected RealTimeTimer timer = new RealTimeTimer();
        protected List<DynamicInfo> selections = new List<DynamicInfo>();
        protected List<MovingData> moving = new List<MovingData>();

        [SerializeField]
        protected SelectionChangedEvent OnSelectionChanged;

        public void Stop() {
            timer.Pause();
            IsActing = false;
            moving.Clear();
        }

        public void ClearSelections() {
            Stop();
            for (int i = 0; i < selections.Count; i++) {
                Destroy(selections[i].gameObject);
            }
            selections.Clear();
            nowSelectedIndex = 0;
        }

        public void AddSelection(params string[] items) {
            if (items.Length % 2 == 1) {
                Debug.LogWarning("[SlideSelector] non matched text-value pairs.");
                return;
            }

            for (int i = 0; i < items.Length; i += 2) {
                AddSelection(items[i], items[i + 1]);
            }
        }

        public void AddSelection(string text, string value) {
            GameObject go;
            if (PrefabSelectionItem != null) {
                go = GameObjectRelate.InstantiateGameObject(SelectionWindow, PrefabSelectionItem);
            } else {
                go = GameObjectRelate.InstantiateGameObject(SelectionWindow, "Item" + text);
            }

            DynamicInfo info = go.GetComponent<DynamicInfo>() ?? go.AddComponent<DynamicInfo>();
            int index = selections.Count;
            info[POSITION_INDEX] = getPositionIndex(index);
            info[VALUE_ID] = value;

            Text txt = go.GetComponent<Text>();
            if (txt != null) {
                txt.text = text;
            }

            selections.Add(info);
            if (selections.Count == 1) {
                SelectTo(0);
            }

            go.transform.position = this[getPositionIndex(index)].position;
            updatePosition();
        }

        public void SetSelection(int index, string text = "", string value = "") {
            if (!TMath.InRange(index, 0, Count - 1) || selections.Count == index) {
                AddSelection(text, value);
                //Debug.LogWarning("[SlideSelector] fail to set info to selection item which index is out of range.");
                return;
            }
            DynamicInfo info = selections[index];
            GameObject go = info.gameObject;
            Text txt = go.GetComponent<Text>();
            if (!string.IsNullOrEmpty(text) && txt != null) {
                txt.text = text;
            }

            if (!string.IsNullOrEmpty(value)) {
                info[VALUE_ID] = value;
            }
        }

        public void RemoveSelectionAfter(int index) {
            int length = Count - index;
            for (int i = 0; i < length; i++) {
                RemoveSelection(Count - i - 1);
            }
        }

        public void RemoveSelection(int index) {
            if (!TMath.InRange(index, 0, Count - 1)) {
                Debug.LogWarning("[SlideSelector] index is out of range.");
                return;
            }
            Destroy(selections[index].gameObject);
            selections.RemoveAt(index);
            if (nowSelectedIndex >= Count) {
                SelectTo(Count - 1);
            }
            updatePosition();
        }

        private int getPositionIndex(int index) {
            return TMath.Range(index - nowSelectedIndex + 1, 0, 2);
        }

        public void ToPrevious() {
            to(false);
        }

        public void ToNext() {
            to(true);
        }

        protected virtual void to(bool toNext) {
            if (IsActing) {
                return;
            }

            int direction = toNext ? 1 : -1;
            int targetIndex = nowSelectedIndex + direction;
            int maxIndex = Count - 1;
            bool canMove = TMath.InRange(targetIndex, 0, maxIndex);
            if (!canMove) {
                return;
            }

            nowSelectedIndex = targetIndex;
            for (int i = 0; i < Count; i++) {
                DynamicInfo info = selections[i];
                GameObject go = info.gameObject;
                int fromIndex = info.Get<int>(POSITION_INDEX);
                int toIndex = getPositionIndex(i);

                if (fromIndex != toIndex) {
                    info[POSITION_INDEX] = toIndex;
                    moving.Add(new MovingData {
                        Target = go,
                        From = this[fromIndex].position,
                        To = this[toIndex].position
                    });
                }
            }

            timer.Start(Interval_sec);
            IsActing = true;
        }

        public void SelectTo(Func<string, bool> selector) {
            SelectTo(selections.FindIndex(d => selector(d[VALUE_ID].ToString())));
        }

        public void SelectTo(int index) {
            nowSelectedIndex = index;
            updatePosition();
        }

        internal void updatePosition() {
            for (int i = 0; i < Count; i++) {
                setPosition(i);
            }
        }

        private void setPosition(int index) {
            DynamicInfo info = selections[index];
            int position = getPositionIndex(index);
            info[POSITION_INDEX] = position;
            info.gameObject.transform.position = this[position].position;
        }

        private void Update() {
            if (IsActing) {
                float progress = timer.GetProgress();
                updateMove(progress);
                if (progress >= 1) {
                    finishMove();
                }
            }
        }

        private void updateMove(float progress) {
            foreach (MovingData info in moving) {
                info.Target.transform.position = Vector3.Lerp(info.From, info.To, progress);
            }
        }

        private void finishMove() {
            IsActing = false;
            moving.Clear();
            for (int i = 0; i < OnSelectionChanged.GetPersistentEventCount(); i++) {
                ((MonoBehaviour)OnSelectionChanged.GetPersistentTarget(i)).SendMessage(OnSelectionChanged.GetPersistentMethodName(i), selections[nowSelectedIndex][VALUE_ID]);
            }
        }
    }

#if UNITY_EDITOR

    [UnityEditor.CustomEditor(typeof(SlideSelector))]
    public class SlideSelectorEditor : UnityEditor.Editor {

        public override void OnInspectorGUI() {
            float offsetX = UnityEditor.EditorGUIUtility.labelWidth - 4;

            SlideSelector slide = target as SlideSelector;
            GUILayout.ExpandWidth(true);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Is Acting", GUILayout.Width(offsetX));
            GUILayout.Label(slide.IsActing.ToString());
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("SelectedIndex", GUILayout.Width(offsetX));
            slide.nowSelectedIndex = UnityEditor.EditorGUILayout.IntField(slide.nowSelectedIndex, GUILayout.Width(40));
            if (GUILayout.Button("Refresh Selection")) {
                slide.updatePosition();
            }
            GUILayout.EndHorizontal();

            DrawDefaultInspector();
        }
    }

#endif
}