﻿using GameLibrary.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.UI {

    /// <summary>動畫UI</summary>
    [ExecuteInEditMode]
    public class SpriteUI : MonoBehaviour {

        /// <summary>Sprite資源所在路徑</summary>
        public string SpriteRoot = "";

        /// <summary>要使用的Sprite</summary>
        public Sprite SpriteUsed;

        /// <summary>動畫總時間</summary>
        public float Duration_sec = .5f;

        /// <summary>是否啟用動畫</summary>
        [SerializeField]
        private bool IsActived = true;

        /// <summary>Sprite資源集合</summary>
        public Sprite[] Sprites;

        /// <summary>現在的幀索引</summary>
        public int frameIndex = 0;

        /// <summary>幀經過時間</summary>
        private float frameDelta = 0f;

        private Image img;

        /// <summary>每幀時間</summary>
        private float frameDuration_sec {
            get {
                return Duration_sec / frameCount;
            }
        }

        /// <summary>幀數</summary>
        private int frameCount {
            get {
                return Sprites.Length;
            }
        }

        private void Start() {
            img = GetComponent<Image>();
            if (Sprites == null || Sprites.Length == 0) {
                LoadSprite(img.sprite);
            }
        }

        public void SetSpritesFromDataShared(string sprite) {
            var atlas = DataShared.Instance.Sprites.GetAtlas(sprite);
            if (atlas != null) {
                SetSprites(atlas.Values);
            }
        }

        public void SetSprites(Sprite[] sprite) {
            Sprites = sprite;
            frameIndex = 0;
            frameDelta = 0;
            changeSprite();
        }

        /// <summary>讀取指定Sprite資源,或重新讀取現有Sprite資源</summary>
        /// <param name="newSprite">指定新Sprite資源,不指定則重新讀取</param>
        public void LoadSprite(string newSpriteName) {
            if (string.IsNullOrEmpty(newSpriteName)) {
                return;
            }
            ResManager<Sprite> atlas = DataShared.Instance.Sprites.GetAtlas(SpriteRoot);
            Sprite[] tmp = atlas != null ? atlas.Values : Resources.LoadAll<Sprite>(SpriteRoot + newSpriteName);
            if (tmp != null && tmp.Length > 0) {
                SpriteUsed = tmp[0];
                Sprites = tmp;
                frameIndex = 0;
                frameDelta = 0;
                changeSprite();
            }
        }

        /// <summary>讀取指定Sprite資源,或重新讀取現有Sprite資源</summary>
        /// <param name="newSprite">指定新Sprite資源,不指定則重新讀取</param>
        public void LoadSprite(Sprite newSprite = null) {
            if (newSprite != null) {
                SpriteUsed = newSprite;
            }
            ResManager<Sprite> atlas = DataShared.Instance.Sprites.GetAtlas(SpriteRoot);
            Sprites = atlas != null ? atlas.Values : Resources.LoadAll<Sprite>(SpriteRoot + SpriteUsed.texture.name);
            frameIndex = 0;
            frameDelta = 0;
            changeSprite();
        }

        /// <summary>停止動畫,並停在第一幀</summary>
        public void Stop() {
            IsActived = false;
            frameIndex = 0;
            changeSprite();
        }

        /// <summary>開始從頭撥放動畫</summary>
        public void Play() {
            IsActived = true;
            frameIndex = 0;
            frameDelta = 0;
            changeSprite();
        }

        private void Update() {
            if (IsActived) {
                updateSprite();
            }
        }

        /// <summary>更新幀</summary>
        private void updateSprite() {
            if (IsActived) {
                frameDelta += Time.deltaTime;
                if (frameDelta > frameDuration_sec) {
                    frameDelta = 0;
                    frameIndex = (frameIndex + 1) % frameCount;
                    changeSprite();
                }
            }
        }

        /// <summary>將圖片更換為Sprites[frameIndex]</summary>
        private void changeSprite() {
            if (img == null) {
                img = GetComponent<Image>();
            }
            frameIndex = TMath.Range(frameIndex, 0, frameCount);
            img.sprite = Sprites[frameIndex];
        }

        public void SetFrame(int index) {
            frameIndex = TMath.Range(index, 0, frameCount);
            changeSprite();
        }
    }
}