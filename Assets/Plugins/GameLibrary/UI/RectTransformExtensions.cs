﻿using UnityEngine;

namespace GameLibrary.UI {

    public struct RectTransformExtensions {

        public static Rect GetScreenSize(Camera camera) {
            return camera.pixelRect;
        }

        public static Rect GetGameRectOnScreen(RectTransform transform) {
            Vector3[] worldCorners = new Vector3[4];
            transform.GetWorldCorners(worldCorners);
            Rect result = new Rect(
                worldCorners[0].x,
                worldCorners[0].y,
                worldCorners[2].x - worldCorners[0].x,
                worldCorners[2].y - worldCorners[0].y);
            return result;
        }

        public static Rect RectTransformToWebSpace(RectTransform rect) {
            Camera camera = Camera.main;
            Canvas canvas = rect.GetComponentInParent<Canvas>();
            float scale = canvas.scaleFactor;
            Vector3 center = camera.WorldToScreenPoint(rect.position);
            Vector2 size = rect.rect.size * scale;

            float left = center.x - size.x * rect.pivot.x;
            float right = center.x + size.x * (1 - rect.pivot.x);
            float top = center.y - size.y * rect.pivot.y;
            float bottom = center.y + size.y * (1 - rect.pivot.y);
            return new Rect(left, Screen.height - bottom, right - left, bottom - top);
        }

#if UNITY_EDITOR

        [UnityEditor.MenuItem("Test/RectTransform/TestRectTransformToWebSpace")]
        public static void TestRectTransformToWebSpace() {
            Camera camera = Camera.main;
            GameObject go = UnityEditor.Selection.activeGameObject;
            Rect result = RectTransformToWebSpace(go.transform as RectTransform);
            Debug.Log("Rect[" + go.name + "]:" + result + "\r\n"
                + "ScreenSize:" + Screen.width + "x" + Screen.height);
        }

        [UnityEditor.MenuItem("Test/RectTransform/ScreenPixelRect")]
        public static void GetScreenPixelRect() {
            Debug.Log("Get Camera.ScreenPixelRect: " + Camera.main.pixelRect);
            Debug.Log("Canvas Scale facotor: " + GameObject.FindObjectOfType<Canvas>().scaleFactor);
        }

#endif
    }
}