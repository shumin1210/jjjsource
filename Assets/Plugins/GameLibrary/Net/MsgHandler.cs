﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.Net {

    public class MsgHandler : MonoBehaviour {
        public const string KEY = "Handler";
        public const string UserTokenKey = "UserToken";
        public static string HandlerID = "Handler";
        public const string ErrorPattern = ".error";
        public bool DisconnectWhenBackground = true;
        protected bool isInBackground = false;

        /// <summary>送出訊息處理式的集合</summary>
        public Dictionary<string, Func<object>> SendHandlers {
            get; protected set;
        }

        /// <summary>接收訊息處理式的集合</summary>
        public Dictionary<string, Action<IDictionary<string, object>>> ReceiveHandlers {
            get; protected set;
        }

        /// <summary>錯誤訊息處理式的集合</summary>
        public Dictionary<string, Action<IDictionary<string, object>>> ErrorHandlers {
            get; protected set;
        }

        protected Action<object> sender;

        protected string unlockType;
        public bool IsAutoReconnectEnabled = false;

        [SerializeField]
        protected Connection connection;

        public MsgHandler() {
            SendHandlers = new Dictionary<string, Func<object>>();
            ReceiveHandlers = new Dictionary<string, Action<IDictionary<string, object>>>();
            ErrorHandlers = new Dictionary<string, Action<IDictionary<string, object>>>();
        }

        public void SetSender(Action<object> send) {
            sender = send;
        }

        public virtual void Disconnect() {
            connection.Close();
        }

        public virtual void Connect() {
            if (connection.state == Connection.States.Closed) {
                connection.StartConnect();
            }
        }

        public virtual void onOpen() {
        }

        public void StartConnecting(string unlockType = "") {
            this.unlockType = unlockType;
            OnStartConnecting();
        }

        public virtual void OnStartConnecting() {
        }

        public void EndConnecting() {
            OnEndConnecting();
            unlockType = string.Empty;
        }

        public virtual void OnEndConnecting() {
        }

        public virtual void onClose() {
            if (IsAutoReconnectEnabled) {
                connection.StartConnect();
            }
        }

        public virtual void onError(string errorMsg = "") {
        }

        public virtual void onMessage(string actType, IDictionary<string, object> content) {
            try {
                if (ReceiveHandlers.ContainsKey(actType)) {
                    ReceiveHandlers[actType](content);
                } else if (actType.Contains(ErrorPattern) && ErrorHandlers.ContainsKey(actType.Split(ErrorPattern)[0])) {
                    ErrorHandlers[actType](content);
                }
            } catch (Exception e) {
                Debug.LogError(e);
            } finally {
                if (!string.IsNullOrEmpty(unlockType) && actType == unlockType || (actType.Split(ErrorPattern)[0] == unlockType)) {
                    EndConnecting();
                }
            }
        }

        public void Send(string actType, object ct) {
            if (sender != null) {
                sender(new ActionObject(actType, ct));
            }
        }

        public void Send(string actType) {
            if (sender != null
                && SendHandlers.ContainsKey(actType)) {
                sender(SendHandlers[actType]());
            }
        }

        public void RegistryHandler(
            string actType,
            Func<object> Send = null,
            Action<IDictionary<string, object>> Receive = null,
            Action<IDictionary<string, object>> Error = null) {
            if (Send != null) {
                if (!SendHandlers.ContainsKey(actType)) {
                    SendHandlers.Add(actType, Send);
                } else {
                    SendHandlers[actType] = Send;

                    //Debug.LogErrorFormat("[MsgHandler] Duplicated sendAction['{0}'].", actType);
                }
            }
            if (Receive != null) {
                if (!ReceiveHandlers.ContainsKey(actType)) {
                    ReceiveHandlers.Add(actType, Receive);
                } else {
                    ReceiveHandlers[actType] = Receive;

                    //Debug.LogErrorFormat("[MsgHandler] Duplicated recvAction['{0}'].", actType);
                }
            }
            if (Error != null) {
                if (!ErrorHandlers.ContainsKey(actType)) {
                    ErrorHandlers.Add(actType, Error);
                } else {
                    ErrorHandlers[actType] = Error;
                }
            }
        }

        private void OnApplicationFocus(bool focus) {
            if (focus && isInBackground) {
                GoToForeground();
            }
        }

        private void OnApplicationPause(bool pause) {
            if (pause && !isInBackground) {
                GoToBackground();
            }
        }

        public void GoToForeground() {
            if (DisconnectWhenBackground) {
                isInBackground = false;
                IsAutoReconnectEnabled = true;
                Connect();
            }
        }

        public void GoToBackground() {
            if (DisconnectWhenBackground) {
                isInBackground = true;
                IsAutoReconnectEnabled = false;
                Disconnect();
            }
        }
    }
}