﻿namespace GameLibrary.Net {

    public class ActionCommand {
        public string actionmaintype;
        public string actionsubtype;
        public string actiontime;
        public object actioncontent;

        public string Type { get { return actionmaintype + "." + actionsubtype; } }

        public ActionCommand() {
        }

        public ActionCommand(string mainType, string subType) {
            actionmaintype = mainType;
            actionsubtype = subType;
            actiontime = "";
            actioncontent = "";
        }

        public ActionCommand(string mainType, string subType, params string[] content) : this(mainType, subType) {
            actioncontent = string.Join(",", content);
        }

        public ActionCommand(string mainType, string subType, object content) : this(mainType, subType) {
            actioncontent = content;
        }

        public ActionCommand(string actType) {
            string[] a = actType.Split('.');
            actionmaintype = a[0];
            actionsubtype = a[1];
            actiontime = "";
            actioncontent = "";
        }

        public ActionCommand(string actType, params string[] content) : this(actType) {
            actioncontent = string.Join(",", content);
        }

        public ActionCommand(string actType, object content) : this(actType) {
            actioncontent = content;
        }
    }
}