﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameLibrary.Utility;
using UnityEngine;

namespace GameLibrary.Net {

    [AddComponentMenu("Network/WebsocketConnection")]
    public class Connection : MonoBehaviour {

        public enum States : ushort {
            Connecting = 0,
            Open = 1,
            Closing = 2,
            Closed = 3
        }

        public enum ActionFormat {

            /// <summary>舊版傳輸格式</summary>
            ActionCommand,

            /// <summary>新版傳輸格式</summary>
            ActionObject
        }

        private WebSocket client;

        public States state = States.Closed;

        [SerializeField]
        private MsgHandler Handler;

        public string URL;

        public ActionFormat Format = ActionFormat.ActionObject;
        public Action<States> OnStateChange;

        private void Awake() {
            if (Handler != null) {
                Handler.SetSender(Send);
                StartConnect();
            }
        }

        private void setState(States newState) {
            if (state != newState) {
                state = newState;

                if (OnStateChange != null) {
                    OnStateChange(newState);
                }
            }
        }

        private void OnApplicationQuit() {
            Close();
        }

        public void StartConnect() {
            if (state != States.Closed) {
                Debug.LogError("abort connection new thread,because og websocket client still connecting.");
                return;
            }
            if (Handler == null) {
                Debug.LogError("handler not asigned.");
                return;
            }
            StartCoroutine(connect());
        }

        private IEnumerator connect() {
            client = new WebSocket(new Uri(URL));
            setState(States.Connecting);
            yield return StartCoroutine(client.Connect());

            setState(States.Open);
            Handler.onOpen();

            while (state == States.Open) {
                string reply = client.RecvString();
                try {
                    if (reply != null) {
                        Debug.Log("[Connection] Receive message" + reply);

                        switch (Format) {
                            case ActionFormat.ActionCommand:
                                handleReceive_ActionCommand(reply);
                                break;

                            case ActionFormat.ActionObject:
                                handleReceive_ActionObject(reply);
                                break;
                        }
                    }
                    if (client.error != null) {
                        setState(States.Closing);
                        Handler.onError(client.error);
                        break;
                    }
                } catch (Exception e) {
                    Debug.LogError(e);
                }

                yield return 0;
            }

            client.Close();
            setState(States.Closed);
            Handler.onClose();

            //client = null;
            yield return 0;
        }

        private void handleReceive_ActionCommand(string reply, bool skip = false) {
            ActionCommand msg = JS.Deserialize<ActionCommand>(reply);
            if (msg != null) {
                if (msg.actioncontent is string) {
                    Handler.onMessage(msg.Type, new Dictionary<string, object>() { { "data", msg.actioncontent } });
                } else {
                    Handler.onMessage(msg.Type, msg.actioncontent as IDictionary<string, object>);
                }
            } else if (!skip) {
                handleReceive_ActionObject(reply, skip: true);
            }
        }

        private void handleReceive_ActionObject(string reply, bool skip = true) {
            ActionObject act = JS.Deserialize<ActionObject>(reply);
            if (act != null) {
                ServerTime.SetServerTime(new DateTime().FromUnix(act.UnixTime));
                Handler.onMessage(act.Type, act.Content);
            } else if (!skip) {
                handleReceive_ActionCommand(reply, skip: true);
            }
        }

        public void Close() {
            if (state == States.Open) {
                client.Close();
                setState(States.Closing);
            }
        }

        public void Send(object act) {
            if (act is ActionObject) {
                Send(act as ActionObject);
            } else if (act is ActionCommand) {
                Send(act as ActionCommand);
            } else if (act is string) {
                Send(act as string);
            } else {
                Send(JS.Serialize(act));
            }
        }

        public void Send(string actype, object content) {
            switch (Format) {
                case ActionFormat.ActionCommand:
                    string[] types = actype.Split('.');
                    Send(JS.Serialize(new ActionCommand(types[0], types[1], content)));
                    break;

                case ActionFormat.ActionObject:
                    Send(JS.Serialize(new ActionObject(actype, content)));
                    break;
            }
        }

        public void Send(ActionObject act) {
            switch (Format) {
                case ActionFormat.ActionCommand:
                    Send(JS.Serialize(new ActionCommand(act.Type, act.Content["Msg"])));
                    break;

                case ActionFormat.ActionObject:
                    Send(JS.Serialize(act));
                    break;
            }
        }

        public void Send(ActionCommand act) {
            switch (Format) {
                case ActionFormat.ActionCommand:
                    Send(JS.Serialize(act));
                    break;

                case ActionFormat.ActionObject:
                    Send(JS.Serialize(new ActionObject(act.Type, act.actioncontent)));
                    break;
            }
        }

        public void Send(string msg) {
            Debug.Log("[Connection] send string: " + msg);
            client.SendString(msg);
        }
    }
}