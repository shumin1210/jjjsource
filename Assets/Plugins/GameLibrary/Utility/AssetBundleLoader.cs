﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace GameLibrary.Utility {

    public class AssetBundleLoader : MonoBehaviour {

        /// <summary>資源包設定</summary>
        public struct AssetBundleConfig {

            /// <summary>Bundel Name</summary>
            public string Name;

            public uint CRC;

            public string Hash;

            public Hash128 HashCode {
                get {
                    return Hash128.Parse(Hash);
                }
            }

            /// <summary>Bundle file name</summary>
            public string FileName {
                get {
                    return Name + Extension;
                }
            }
        }

        #region Const

        private const string Extension = ".data";
        private const string configName = "configs.json";
        private const string urlPattern = "{0}/{1}/{2}/";

        #endregion Const

        #region 設定

        /// <summary>載入資源的狀態</summary>
        public string StateText = "";

        /// <summary>資源網址</summary>
        public string ResPath = "http://obacart.7sbc888.com.win/u/AssetBundles";

        /// <summary>資源分區代號</summary>
        public string Variant = "";

        /// <summary>平台(唯讀)</summary>
        public string platform = "windows";

        /// <summary>本地路徑(未啟用)</summary>
        public string pathLocal;

        /// <summary>重載遊戲資源包時不重載的資源</summary>
        public List<string> keep = new List<string>();

        /// <summary>仔入資源時,忽略的資源(多為共用,通用資源)</summary>
        public List<string> ignores = new List<string>() { "shared" };

        #endregion 設定

        #region 事件

        /// <summary>事件:讀取進度更新</summary>
        public Action<float, AssetBundleConfig> OnProgress;

        /// <summary>事件:完成載入一個資源包</summary>
        public Action<AssetBundle> OnCompleteOne;

        /// <summary>事件:完成載入全部資源包</summary>
        public Action<Dictionary<string, AssetBundle>> OnCompleteAll;

        /// <summary>事件:仔入資源包發生錯誤(無法連線,遠端資源不存在等)</summary>
        public Action<AssetBundleConfig> OnError;

        #endregion 事件

        /// <summary>依據遊戲ID分類的資源包集合</summary>
        public Dictionary<string, Dictionary<string, AssetBundle>> GameAssets {
            get; protected set;
        }

        private UnityWebRequest req;

        public AssetBundleLoader() {
            GameAssets = new Dictionary<string, Dictionary<string, AssetBundle>>();
        }

        #region Unity方法

        private void OnDisable() {
            if (req != null) {
                req.Dispose();
            }
        }

        #endregion Unity方法

        /// <summary>取得刪減分區代碼後的資源包名稱字串</summary>
        /// <param name="ab">資源包物件</param>
        /// <returns>資源包名稱</returns>
        public static string TrimAssetBundleVariantName(AssetBundle ab) {
            if (ab.name.Contains(".")) {
                return ab.name.Substring(0, ab.name.LastIndexOf("."));
            } else {
                return ab.name;
            }
        }

        /// <summary>指定事件</summary>
        /// <param name="onProgress">指派事件:進度更新</param>
        /// <param name="onCompleteOne">指派事件:完成載入單個資源</param>
        /// <param name="onCompleteAll">指派事件:完成載入全部資源</param>
        /// <param name="onError">指派事件:載入失敗</param>
        public void SetEvents(Action<float, AssetBundleConfig> onProgress = null
            , Action<AssetBundle> onCompleteOne = null
            , Action<Dictionary<string, AssetBundle>> onCompleteAll = null
            , Action<AssetBundleConfig> onError = null) {
            OnProgress = onProgress;
            OnCompleteOne = onCompleteOne;
            OnCompleteAll = onCompleteAll;
            OnError = onError;
        }

        /// <summary>開始載入指定遊戲的所有資源包</summary>
        /// <param name="gameid">指定遊戲ID</param>
        /// <param name="path">遠端路徑</param>
        /// <param name="variant">分區代碼</param>
        /// <param name="onProgress">指派事件:進度更新</param>
        /// <param name="onCompleteOne">指派事件:完成載入單個資源</param>
        /// <param name="onCompleteAll">指派事件:完成載入全部資源</param>
        /// <param name="onError">指派事件:載入失敗</param>
        public void StartLoad(string gameid, string path = null, string variant = null
            , Action<float, AssetBundleConfig> onProgress = null
            , Action<AssetBundle> onCompleteOne = null
            , Action<Dictionary<string, AssetBundle>> onCompleteAll = null
            , Action<AssetBundleConfig> onError = null) {
            SetEvents(onProgress, onCompleteOne, onCompleteAll, onError);
            ResPath = path ?? ResPath;
            Variant = variant ?? Variant;
            StartCoroutine(LoadGameAssets(gameid));
        }

        /// <summary>載入單個資源包</summary>
        /// <param name="name">資源包名稱</param>
        /// <param name="hash">資源包的hash值</param>
        /// <param name="CRC">資源包的CRC值</param>
        /// <param name="url">遠端路徑</param>
        /// <param name="gameID">資源包所屬的遊戲ID</param>
        /// <returns>coroutine</returns>
        public IEnumerator LoadAssetBundle(string name, string hash, uint CRC, string url, string gameID) {
            yield return LoadAssetBundle(new AssetBundleConfig { Name = name, Hash = hash, CRC = CRC }, url, gameID);
        }

        /// <summary>載入單個資源包</summary>
        /// <param name="abConfig">資源包設定檔</param>
        /// <param name="url">遠端路徑</param>
        /// <param name="gameID">資源包所屬的遊戲ID</param>
        /// <returns>coroutine</returns>
        public IEnumerator LoadAssetBundle(AssetBundleConfig abConfig, string url, string gameID) {
            long fileSize = 0;
            yield return getFileSize(abConfig, url, s => fileSize = s);
            fileSize /= 1024;

            if (fileSize == 0) {
                if (OnError != null) {
                    OnError(abConfig);
                }
            }

            using (UnityWebRequest req = UnityWebRequest.GetAssetBundle(url + abConfig.Name + Extension, abConfig.HashCode, abConfig.CRC)) {
                this.req = req;
                req.disposeDownloadHandlerOnDispose = true;
                AsyncOperation request = req.Send();

                bool cached = Caching.IsVersionCached(req.url, abConfig.HashCode);
                Debug.Log("start download " + abConfig.Name);

                while (!request.isDone) {
                    if (cached) {
                        StateText = "Decompressing resource[{0}].".format(abConfig.Name);
                        OnProgress(1f, abConfig);
                        yield return new WaitForEndOfFrame();
                    } else {
                        StateText = "Loading resource[{3}]: {0}/{1}KB ({2:0.#}%)".format(request.progress * fileSize, fileSize, request.progress * 100, abConfig.Name);
                        if (OnProgress != null) {
                            OnProgress(request.progress / .9f, abConfig);
                        }
                        yield return null;
                    }
                }

                StateText = "Loading resource[{0}] complete.".format(abConfig.Name);
                if (OnProgress != null) {
                    OnProgress(1f, abConfig);
                }

                yield return null;
                AssetBundle ab = DownloadHandlerAssetBundle.GetContent(req);
                addToGameAssets(gameID, ab);

                if (OnCompleteOne != null) {
                    OnCompleteOne(ab);
                }
            }
            this.req = null;
        }

        private void addToGameAssets(string gameID, AssetBundle ab) {
            string abName = TrimAssetBundleVariantName(ab);

            if (GameAssets[gameID].ContainsKey(abName)) {
                GameAssets[gameID][abName] = ab;
            } else {
                GameAssets[gameID].Add(abName, ab);
            }

            VariantAssetManager.Instance.Refresh(abName, ab);
        }

        /// <summary>載入指定遊戲的所有資源</summary>
        /// <param name="gameID">指定遊戲ID</param>
        /// <returns>coroutine</returns>
        public IEnumerator LoadGameAssets(string gameID) {
            switchPlatform();
            refreshGameAssets(gameID);

            List<AssetBundleConfig> map = new List<AssetBundleConfig>();
            string url = urlPattern.format(ResPath, platform, gameID);
            pathLocal = Application.persistentDataPath + "/AssetBundles/" + gameID + "/";

            #region get config

            using (UnityWebRequest rqConfig = UnityWebRequest.Get(url + configName)) {
                yield return rqConfig.Send();

                map = getMap(map, rqConfig);
            }

            #endregion get config

            foreach (AssetBundleConfig item in map) {
                yield return LoadAssetBundle(item, url, gameID);
            }
            if (OnCompleteAll != null) {
                OnCompleteAll(GameAssets[gameID]);
            }
        }

        private void refreshGameAssets(string gameID) {
            if (!GameAssets.ContainsKey(gameID)) {
                GameAssets.Add(gameID, new Dictionary<string, AssetBundle>());
            } else {
                GameAssets[gameID].Foreach(s => s.Value.Unload(true));
                GameAssets[gameID].Clear();
            }
        }

        private void switchPlatform() {
#if UNITY_WEBGL && !UNITY_EDITOR
            platform = "webgl";
#elif UNITY_WINDOWS || UNITY_MAC || UNITY_EDITOR
            platform = "windows";
#elif UNITY_ANDROID
            platform = "android";
#elif UNITY_IOS
            platform = "ios";
#endif
        }

        private IEnumerator getFileSize(AssetBundleConfig abConfig, string url, Action<long> result) {
            long fileSize = 0;
            using (UnityWebRequest headRequest = UnityWebRequest.Head(url + abConfig.Name + Extension)) {
                yield return headRequest.Send();
                if (headRequest.responseCode != 200) {
                    // TODO: Error response
                } else {
                    string contentLength = headRequest.GetResponseHeader("CONTENT-LENGTH");
                    long.TryParse(contentLength, out fileSize);
                }
            }
            result(fileSize);
        }

        private List<AssetBundleConfig> getMap(List<AssetBundleConfig> map, UnityWebRequest rqConfig) {
            if (rqConfig.responseCode != 200) {
                // TODO: Error response
            } else {
                map = DynamicParser.ToDynamicList(JS.Deserialize(rqConfig.downloadHandler.text)["AssetBundles"])
                        .SelectWhere(s => new AssetBundleConfig {
                            Name = s.Get("Name"),
                            Hash = s.Get("Hash"),
                            CRC = s.Get<uint>("CRC")
                        }, s => {
                            //ignore (shared) asset bundle
                            if (ignores.Contains(s.Name)) {
                                return false;
                            }
                            string[] names = s.Name.Split(".");
                            if (names.Length > 1) {
                                //check variant bundles
                                return (names[names.Length - 1] == Variant);
                            } else {
                                //normal bundles
                                return true;
                            }
                        }).ToList();
            }

            return map;
        }
    }
}