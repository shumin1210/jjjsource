﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameLibrary.Utility {

    /// <summary>將物件轉換成指定類型,以方便取用物件內容,可配合JSON反序列化後的物件使用</summary>
    public static class DynamicParser {

        /// <summary>轉換成字串Dictionary</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>value型別為string的Dictionary</returns>
        public static Dictionary<string, string> ToDictionary(object src) {
            IDictionary<string, object> tmp = (src as IDictionary<string, object>);
            if (tmp == null) {
                return null;
            }
            return tmp.ToDictionary(d => d.Key, d => d.Value.ToString());
        }

        /// <summary>轉換成指定value型別Dictionary物件</summary>
        /// <typeparam name="T">指定value型別,僅接受實質型別</typeparam>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>Dictionary物件</returns>
        public static Dictionary<string, T> ToDictionary<T>(object src) where T : struct {
            IDictionary<string, object> tmp = (src as IDictionary<string, object>);
            if (tmp == null) {
                return null;
            }
            return tmp.ToDictionary(d => d.Key, d => (T)Convert.ChangeType(d.Value, typeof(T)));
        }

        /// <summary>轉換成Dictionary類型的動態物件</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>Dictionary物件</returns>
        public static IDictionary<string, object> ToDynamic(object src) {
            return src as IDictionary<string, object>;
        }

        /// <summary>轉換成元素為字串的List</summary>
        public static List<string> ToList(object src) {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => d.ToString()).ToList();
        }

        /// <summary>轉換成指定元素型別的List物件</summary>
        /// <typeparam name="T">元素的型別</typeparam>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static List<T> ToList<T>(object src) where T : struct {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => (T)Convert.ChangeType(d, typeof(T))).ToList();
        }

        /// <summary>轉換成指定元素型別的List物件</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static String[] ToArray(object src) {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => d.ToString()).ToArray();
        }

        /// <summary>轉換成指定元素型別的Array物件</summary>
        /// <typeparam name="T">元素的型別</typeparam>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static T[] ToArray<T>(object src) where T : struct {
            IList<object> tmp = (src as IList<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => (T)Convert.ChangeType(d, typeof(T))).ToArray();
        }

        /// <summary>轉換成動態元素的List物件</summary>
        /// <param name="src">要轉換的來源物件</param>
        /// <returns>List物件</returns>
        public static List<IDictionary<string, object>> ToDynamicList(object src) {
            List<object> tmp = (src as List<object>);
            if (tmp == null) {
                return null;
            }
            return tmp.Select(d => d as IDictionary<string, object>).ToList();
        }

        private static object tryGet(IDictionary<string, object> src, string key, object defaultVal) {
            object value;
            if (src.TryGetValue(key, out value)) {
                return value;
            } else {
                Debug.LogErrorFormat("[DynamicParser] key:{0} is not in target.", key);
                return defaultVal;
            }
        }

        public static string Get(this IDictionary<string, object> src, string key) {
            return tryGet(src, key, "").ToString();
        }

        public static bool TryGet(this IDictionary<string, object> src, string key, ref string result) {
            object value;
            bool find = src.TryGetValue(key, out value);
            if (find) {
                result = value.ToString();
            }
            return find;
        }

        public static bool TryGet<T>(this IDictionary<string, object> src, string key, ref T result) {
            object value;
            bool find = src.TryGetValue(key, out value);
            if (find) {
                result = TConvert.ToGeneric<T>(value);
            }
            return find;
        }

        public static T Get<T>(this IDictionary<string, object> src, string key) {
            return TConvert.ToGeneric<T>(tryGet(src, key, default(T)));
        }

        public static Dictionary<string, T> Cast<T>(this IDictionary<string, object> src) where T : struct {
            return ToDictionary<T>(src);
        }

        public static Dictionary<string, String> Cast(this IDictionary<string, object> src) {
            return ToDictionary(src);
        }
    }
}