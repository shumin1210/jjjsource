﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    public struct TMath {

        public static bool InRange(int source, int min, int max) {
            if (min > max) {
                int tmp = max;
                max = min;
                min = tmp;
            }
            return source >= min && source <= max;
        }

        /// <summary>回傳不低於min,且不超過max的值</summary>
        /// <param name="source">來源值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>區間內的值</returns>
        public static int Range(int source, int min, int max) {
            if (min > max) {
                int tmp = max;
                max = min;
                min = tmp;
            }

            return Mathf.Min(Mathf.Max(min, source), max);
        }

        /// <summary>回傳不低於min,且不超過max的值</summary>
        /// <param name="source">來源值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>區間內的值</returns>
        public static float Range(float source, float min, float max) {
            if (min > max) {
                float tmp = max;
                max = min;
                min = tmp;
            }

            return Mathf.Min(Mathf.Max(min, source), max);
        }

        public static long Factorial(long end) {
            long result = 1;
            for (long i = 1; i <= end; i++) {
                result *= i;
            }

            return result;
        }

        public static long Factorial(long start, long end) {
            long result = 1;
            for (long i = start; i <= end; i++) {
                result *= i;
            }

            return result;
        }

        public static long CRN(long n, long r) {
            /* n!/((n-m)!*m!)=> (n*n1*n2...m)/(n-m)! */
            return Factorial(r, n) / Factorial(n - r);
        }

        /// <summary>
        /// Cn取m
        /// </summary>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static long C(int n, int m) {
            //Cn取m = n! / (n-m)! m!
            long result = 1;
            //n!/(n-m)!
            for (int i = n; i > (n - m); i--) {
                result *= i;
            }
            result /= (int)Factorial(m);

            return result;
        }
    }
}