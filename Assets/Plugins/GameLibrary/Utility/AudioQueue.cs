﻿using System;
using System.Collections;
using UnityEngine;

namespace GameLibrary.Utility {

    [RequireComponent(typeof(AudioSource))]
    public class AudioQueue : MonoBehaviour {

        [SerializeField]
        private AudioClip[] clips;

        public float delay_sec = .3f;
        private AudioSource source;
        private bool stop;
        private Coroutine thread;
        private Action callback;

        private void Start() {
            source = gameObject.GetComponent<AudioSource>();
        }

        public void Play(AudioClip[] clips, Action onComplete = null) {
            source.Stop();
            stop = false;
            this.clips = clips;
            callback = onComplete;
            thread = StartCoroutine(playQueue());
        }

        public void Play(params AudioClip[] clips) {
            Play(clips, null);
        }

        public void Stop() {
            stop = true;

            source.Stop();
        }

        private IEnumerator playQueue() {
            foreach (AudioClip clip in clips) {
                if (!stop) {
                    source.clip = clip;
                    source.Play();
                    yield return new WaitForSeconds(clip.length + delay_sec);
                }
            }
            if (callback != null) {
                callback();
            }
        }
    }
}