﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameLibrary.Utility {

    /// <summary>Lniq擴充方法</summary>
    public static class LinQExtensions {

        /// <summary>判斷集合中的元素是否全部不重複</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src">要判斷的集合</param>
        /// <returns></returns>
        public static bool IsDistinct<T>(this IEnumerable<T> src) {
            HashSet<T> keys = new HashSet<T>();
            foreach (T item in src) {
                if (keys.Contains(item)) {
                    return false;
                } else {
                    keys.Add(item);
                }
            }

            return true;
        }

        /// <summary>返回來源集合的不重複的元素</summary>
        /// <typeparam name="TElement">元素型別</typeparam>
        /// <typeparam name="TKey">元素判斷值的型，將依據此值比對元素間是否相等</typeparam>
        /// <param name="source">來源集合</param>
        /// <param name="keySelector">取得元素判斷值的方法</param>
        /// <returns></returns>
        public static IEnumerable<TElement> Distinct<TElement, TKey>(this IEnumerable<TElement> source, Func<TElement, TKey> keySelector) {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TElement element in source) {
                TKey elementValue = keySelector(element);
                if (seenKeys.Add(elementValue)) {
                    yield return element;
                }
            }
        }

        /// <summary>降低維度</summary>
        /// <param name="src">來源陣列物件</param>
        public static object[] DownGrade(this object[] src) {
            object[] arr = new object[0];
            for (int i = 0; i < src.Count(); i++) {
                object obj = src.ElementAt(i);
                if (obj != null && src.ElementAt(i).GetType().IsArray) {
                    arr = arr.Concat(obj as object[]).ToArray();
                } else {
                    arr = arr.Concat(new object[] { obj }).ToArray();
                }
            }
            return arr;
        }

        /// <summary>降低維度</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="src">來源陣列物件</param>
        public static T[] DownGrade<T>(this IEnumerable<T[]> src) {
            T[] arr = new T[0];
            if (src.Count() > 0 && src.ElementAt(0) is T[]) {
                for (int i = 0; i < src.Count(); i++) {
                    arr = arr.Concat(src.ElementAt(i)).ToArray();
                }
            }

            return arr;
        }

        /// <summary>降低維度</summary>
        /// <typeparam name="Element">元素型別</typeparam>
        /// <param name="src">來源陣列物件</param>
        public static Output[] Cast<Element, Output>(this IEnumerable<Element> src, Func<Element, Output> covertor) {
            return src.Select(covertor).ToArray();
        }

        /// <summary>For迴圈Linq版</summary>
        /// <typeparam name="Element">集合元素</typeparam>
        /// <param name="src">集合來源</param>
        /// <param name="action">回圈內對每個元素的行為</param>
        public static void Foreach<Element>(this IEnumerable<Element> src, Action<Element> action) {
            for (int i = 0; i < src.Count(); i++) {
                action(src.ElementAt(i));
            }
        }

        /// <summary>For迴圈Linq版</summary>
        /// <typeparam name="Element">集合元素</typeparam>
        /// <param name="src">集合來源</param>
        /// <param name="action">回圈內對每個元素的行為</param>
        public static void Foreach<Element>(this IEnumerable<Element> src, Action<Element, int> action) {
            for (int i = 0; i < src.Count(); i++) {
                action(src.ElementAt(i), i);
            }
        }

        public static IEnumerable<Output> SelectWhere<Element, Output>(this IEnumerable<Element> src, Func<Element, Output> selector, Func<Output, bool> restrict) {
            List<Output> result = new List<Output>();
            for (int i = 0; i < src.Count(); i++) {
                Element e = src.ElementAt(i);
                Output o = selector(e);
                if (restrict(o)) {
                    result.Add(o);
                }
            }
            return result;
        }

        public static IEnumerable<Output> SelectWhere<Element, Output>(this IEnumerable<Element> src, Func<Element, bool> restrict, Func<Element, Output> selector) {
            List<Output> result = new List<Output>();
            for (int i = 0; i < src.Count(); i++) {
                Element e = src.ElementAt(i);
                if (restrict(e)) {
                    result.Add(selector(e));
                }
            }
            return result;
        }

        /// <summary>尋找符合條件的元素的索引值</summary>
        /// <typeparam name="T">集合元素</typeparam>
        /// <param name="src">集合來源</param>
        /// <param name="condition">條件判斷式</param>
        /// <returns>符合條件的索引值集合</returns>
        public static IEnumerable<int> FindIndexes<T>(this IEnumerable<T> src, Func<T, bool> condition) {
            List<int> indexes = new List<int>();
            for (int i = 0; i < src.Count(); i++) {
                if (condition(src.ElementAt(i))) {
                    indexes.Add(i);
                }
            }
            return indexes;
        }

        /// <summary>從指定索引值開始向前/後遍歷,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static void LoopEach<T>(this IEnumerable<T> src, Action<T> act, int startIndex = 0, bool increasing = true) {
            LoopEach(src, (d, i) => act(d), startIndex, increasing);
        }

        /// <summary>從指定索引值開始向前/後遍歷,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static void LoopEach<T>(this IEnumerable<T> src, Action<T, int> act, int startIndex = 0, bool increasing = true) {
            int len = src.Count();
            if (increasing) {
                for (int i = startIndex; i < len; i++) {
                    act(src.ElementAt(i), i);
                }
                for (int i = 0; i < startIndex; i++) {
                    act(src.ElementAt(i), i);
                }
            } else {
                for (int i = startIndex; i >= 0; i--) {
                    act(src.ElementAt(i), i);
                }
                for (int i = len - 1; i > startIndex; i--) {
                    act(src.ElementAt(i), i);
                }
            }
        }

        /// <summary>從指定索引值開始向前/後尋找符合條件的元素,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static T LoopFind<T>(this IEnumerable<T> src, Func<T, bool> act, int startIndex = 0, bool increasing = true) {
            return LoopFind(src, (d, i) => act(d), startIndex, increasing);
        }

        /// <summary>從指定索引值開始向前/後尋找符合條件的元素,遇到末端(開頭或結尾)會從反向的末端繼續遍歷</summary>
        /// <typeparam name="T">子元素</typeparam>
        /// <param name="src">來源集合</param>
        /// <param name="act">遍歷動作</param>
        /// <param name="startIndex">開始索引值</param>
        /// <param name="increasing">是否為向後遍立</param>
        public static T LoopFind<T>(this IEnumerable<T> src, Func<T, int, bool> act, int startIndex = 0, bool increasing = true) {
            int len = src.Count();
            if (increasing) {
                for (int i = startIndex; i < len; i++) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
                for (int i = 0; i < startIndex; i++) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
            } else {
                for (int i = startIndex; i >= 0; i--) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
                for (int i = len - 1; i > startIndex; i--) {
                    if (act(src.ElementAt(i), i)) {
                        return src.ElementAt(i);
                    }
                }
            }
            return default(T);
        }
    }
}