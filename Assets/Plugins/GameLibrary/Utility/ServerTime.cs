﻿using System;

namespace GameLibrary.Utility {

    /// <summary>伺服器時間</summary>
    public static class ServerTime {

        /// <summary>時間誤差</summary>
        private static TimeSpan dif;

        /// <summary>伺服器現在時間</summary>
        public static DateTime Now {
            get {
                return DateTime.Now - dif;
            }
        }

        /// <summary>設定伺服器時間</summary>
        /// <param name="time">伺服器本地時間</param>
        public static void SetServerTime(DateTime time) {
            dif = DateTime.Now - time;
        }

        public static DateTime FromDateTime(DateTime time) {
            return time + dif;
        }

        /// <summary>轉換成伺服器時間(本地時區)</summary>
        /// <param name="timestamp">要轉換的時間戳記(本地時區)</param>
        /// <returns>對應的伺服器時間(本地時區)</returns>
        public static DateTime ConvertFromUnix(long timestamp) {
            return DateTimeExtensions.FromUnix(timestamp);
        }

        /// <summary>轉換成伺服器時間(本地時區)</summary>
        /// <param name="dateString">來自伺服器的時間字串,unixtimestamp字串或標準時間字串</param>
        /// <returns>對應的伺服器時間(本地時區)</returns>
        public static DateTime ConvertFromServerDateString(string dateString) {
            return DateTimeExtensions.FromUnix(dateString);
        }
    }
}