﻿using System;
using System.Text.RegularExpressions;

namespace GameLibrary.Utility {

    /// <summary>擴充型別轉換器</summary>
    public static class TConvert {

        /// <summary>轉出為多型</summary>
        /// <typeparam name="T">轉出的型別</typeparam>
        /// <param name="target">要轉換的物件</param>
        /// <param name="useException">是否在轉換失敗時丟出例外</param>
        /// <returns>轉出後的物件</returns>
        public static T ToGeneric<T>(object target, bool useException = false) {
            if (target is T) {
                return (T)target;
            }

            if (target != null) {
                Type type = typeof(T);
                Type oType = target.GetType();

                try {
                    if (target is decimal) {
                        System.Reflection.MethodInfo toT = typeof(decimal).GetMethod("To" + typeof(T).Name);
                        if (toT != null) {
                            return (T)toT.Invoke(null, new object[] { target });
                        }
                    } else if (type.IsValueType) {
                        /*實質型別=>TryParse*/
                        object tester = oType.IsValueType ? target.ToString() : target;
                        Type[] Types = { typeof(string), type.MakeByRefType() };
                        System.Reflection.MethodInfo tryParse = type.GetMethod("TryParse", Types);
                        if (tryParse != null) {
                            object[] args = { tester, null };
                            if ((bool)tryParse.Invoke(null, args)) {
                                return (T)args[1];
                            }
                        }
                    } else {/*參考型別=>ChangeType*/
                        T result = (T)Convert.ChangeType(target, typeof(T));
                        return result;
                    }
                } catch (InvalidCastException e) {
                    if (useException) {
                        throw e;
                    }
                } catch (FormatException e) {
                    if (useException) {
                        throw e;
                    }
                } catch (ArgumentException e) {
                    if (useException) {
                        throw e;
                    }
                }
            }
            return default(T);
        }

        /// <summary>轉換為Long(int64)物件</summary>
        /// <param name="o">要轉換的物件</param>
        /// <returns>轉換後的物件</returns>
        public static long ToLong(object o) {
            return ToLong(o, 0);
        }

        /// <summary>轉換為Long(int64)物件</summary>
        /// <param name="o">要轉換的物件</param>
        /// <param name="defaultValue">轉換失敗時回傳的預設值</param>
        /// <returns>轉換後的物件</returns>
        public static long ToLong(object o, long defaultValue) {
            if (o == null) {
                return defaultValue;
            }

            if (o is Int64) {
                return (Int64)o;
            }

            string str = o.ToString();
            long result = defaultValue;
            if (!Int64.TryParse(str, out result) && str.Length > 0) {
                double tmp;
                if (double.TryParse(str, out tmp)) {
                    result = (int)tmp;
                } else {
                    str = new Regex("/[^0-9]/g").Replace(str, "");
                    Int64.TryParse(str, out result);
                }
            }
            return result;
        }

        /// <summary>轉換為Int(int32)物件</summary>
        /// <param name="o">要轉換的物件</param>
        /// <returns>轉換後的物件</returns>
        public static int ToInt(object o) {
            return ToInt(o, 0);
        }

        /// <summary>轉換為Long(int64)物件</summary>
        /// <param name="o">要轉換的物件</param>
        /// <param name="defaultValue">轉換失敗回傳的預設值</param>
        /// <returns>轉換後的物件</returns>
        public static int ToInt(object o, int defaultValue) {
            if (o == null) {
                return defaultValue;
            }

            if (o is Int32) {
                return (int)o;
            }

            string str = o.ToString();
            int result = defaultValue;
            if (!Int32.TryParse(str, out result) && str.Length > 0) {
                double tmp;
                if (double.TryParse(str, out tmp)) {
                    result = (int)tmp;
                } else {
                    str = new Regex("/[^0-9]/g").Replace(str, "");
                    Int32.TryParse(str, out result);
                }
            }
            return result;
        }

        /// <summary>轉換為Boolean物件</summary>
        /// <param name="o">要轉換的物件</param>
        /// <returns>轉換後的物件</returns>
        public static bool ToBoolean(object o) {
            if (o == null) {
                return false;
            }

            if (o is bool) {
                return (bool)o;
            }

            string str = o.ToString();
            int num;
            bool result = false;
            if (int.TryParse(str, out num)) {
                result = num != 0;
            } else if (!bool.TryParse(str, out result) && str.Length > 0) {
                result = true;
            } else if (!result && o != null) {
                result = true;
            }

            return result;
        }

        /// <summary>轉換為字串物件</summary>
        /// <param name="o">要轉換的物件</param>
        /// <returns>轉換後的物件</returns>
        public static string ToString(object o) {
            if (o == null) {
                return "";
            }

            return o.ToString();
        }
    }
}