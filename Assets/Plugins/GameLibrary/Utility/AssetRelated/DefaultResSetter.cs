﻿using GameLibrary.UI;
using UnityEngine;

namespace GameLibrary.Utility {

    public class DefaultResSetter : MonoBehaviour {
        public Font DefaultFont;
        public GameObject RootCanvas;

        private void Start() {
            DefaultResManager.DefaultFont = DefaultFont;
            UIManager.Instance.CanvasRoot = RootCanvas;
        }
    }
}