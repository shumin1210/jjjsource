﻿using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Utility {

    public class VariantElement : MonoBehaviour {
        public string ResName;

        public VariantAssetManager.AssetType Type;

        public string BundleName;

        public bool AutoHide = true;

        public void ApplySetting() {
            switch (Type) {
                case VariantAssetManager.AssetType.Image:
                    Image img = GetComponent<Image>();
                    if (img) {
                        ResName = img.sprite ? img.sprite.name : ResName;
                    }
                    break;

                default:
                    break;
            }
        }

        private void Awake() {
            VariantAssetManager.Instance.Registry(this);
        }

        private void OnDestroy() {
            VariantAssetManager.Instance.Deregistry(this);
        }

        internal void RefreshAsset(AssetBundle ab) {
            switch (Type) {
                case VariantAssetManager.AssetType.Image:
                    Image image = GetComponent<Image>();
                    if (image) {
                        image.sprite = ab.LoadAsset<Sprite>(ResName) ?? image.sprite;
                        if (AutoHide) {
                            image.gameObject.SetActive(image.sprite);
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }
}