﻿using System.Linq;
using UnityEngine;

namespace GameLibrary.Utility {

    public class ResManager<T> where T : UnityEngine.Object {
        protected TDictionary<string, T> data = new TDictionary<string, T>(TDictionary.MODE.AUTO);

        public virtual T this[object key] {
            get {
                return data[key.ToString()];
            }
            set {
                data[key.ToString()] = value;
            }
        }

        public string[] Keys {
            get {
                return data.Keys.ToArray();
            }
        }

        public T[] Values {
            get {
                return data.Values.ToArray();
            }
        }

        public virtual T Set(string key, T res) {
            return data[key] = res;
        }

        public virtual T Get(string key) {
            return data[key];
        }

        public virtual void Clear() {
            data.Foreach(o => Resources.UnloadAsset(o.Value));
            data.Clear();
        }

        public virtual T Load(T source) {
            return this[source.name] = source;
        }

        public virtual T Load(string source) {
            if (!this[source]) {
                this[source] = Resources.Load<T>(source);
            }
            return this[source];
        }

        public virtual T[] Load(params T[] source) {
            return source.SelectWhere(o => Load(o), o => o != null).ToArray();
        }

        public virtual T[] Load(params string[] source) {
            return source.SelectWhere(o => Load(o), o => o != null).ToArray();
        }
    }

    public class SpriteResManager : ResManager<Sprite> {
        protected TDictionary<string, ResManager<Sprite>> atlases = new TDictionary<string, ResManager<Sprite>>();

        public ResManager<Sprite> SetAtlas(string root, params Sprite[] sprites) {
            var atlas = atlases[root] = atlases[root] ?? new ResManager<Sprite>();
            sprites.Foreach(o => atlas[o.name] = o);
            return atlas;
        }

        public ResManager<Sprite> GetAtlas(string root) {
            var atlas = atlases[root];
            if (!atlases.ContainsKey(root)) {
                atlas = LoadAtlas(root);
            }
            return atlas;
        }

        public ResManager<Sprite> LoadAtlas(string root) {
            var atlas = atlases[root] ?? new ResManager<Sprite>();
            Resources.LoadAll<Sprite>(root).Foreach(s => atlas[s.name] = s);
            return atlas;
        }

        public override void Clear() {
            atlases.Foreach(a => a.Value.Clear());
            atlases.Clear();
            base.Clear();
        }
    }
}