﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameLibrary.Utility {

    public class VariantAssetManager : Singleton<VariantAssetManager> {

        public enum AssetType {
            Image,
        }

        private DataManager map = new DataManager();

        public void Registry(VariantElement target) {
            Registry(target.BundleName, target);
        }

        public void Registry(string bundleName, VariantElement target) {
            map.Set(new string[] { bundleName, target.name }, target);
        }

        public void Deregistry(VariantElement target) {
            Deregistry(target.BundleName, target);
        }

        public void Deregistry(string bundleName, VariantElement target) {
            map.Remove(new string[] { bundleName, target.name });
        }

        public void Refresh(string bundleName, AssetBundle ab) {
            if (map.ContainsKey(bundleName)) {
                map.Get<IDictionary<string, object>>(bundleName).Foreach(s => (s.Value as VariantElement).RefreshAsset(ab));
            }
        }
    }
}