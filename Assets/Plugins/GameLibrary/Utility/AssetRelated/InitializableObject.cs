﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    public interface IInitializable {

        void OnResInitialized();
    }

    /// <summary>提供資源初始化後,呼叫關聯資源更新的方法</summary>
    public static class ResourceInitializer {
        private static Dictionary<Type, HashSet<IInitializable>> dictionary = new Dictionary<Type, HashSet<IInitializable>>();

        /// <summary>將物件加入關聯集合</summary>
        /// <param name="type">關聯類型</param>
        /// <param name="obj">要加入集合的物件</param>
        public static void Add(Type type, IInitializable obj) {
            if (!dictionary.ContainsKey(type)) {
                dictionary.Add(type, new HashSet<IInitializable>());
            }
            dictionary[type].Add(obj);
        }

        /// <summary>從關聯集合中移除物件</summary>
        /// <param name="type">關聯類型</param>
        /// <param name="obj">要移除的物件</param>
        public static void Remove(Type type, IInitializable obj) {
            if (dictionary.ContainsKey(type)) {
                dictionary[type].Remove(obj);

                if (dictionary[type].Count == 0) {
                    dictionary.Remove(type);
                }
            }
        }

        /// <summary>清空關聯物件集合</summary>
        /// <param name="type">關聯類型</param>
        public static void RemoveAll(Type type) {
            if (dictionary.ContainsKey(type)) {
                dictionary[type].Clear();

                dictionary.Remove(type);
            }
        }

        /// <summary>更新所有關聯物件</summary>
        public static void Initialize() {
            foreach (var item in dictionary) {
                Initialize(item.Key);
            }
        }

        /// <summary>更新指令關聯類型的物件</summary>
        /// <param name="type">關聯類型</param>
        public static void Initialize(Type type) {
            if (dictionary.ContainsKey(type)) {
                foreach (var item in dictionary[type]) {
                    if (item != null) {
                        item.OnResInitialized();
                    }
                }
            }
        }
    }

    /// <summary>提供特定資源完成初始化時,更新關聯物件的方法</summary>
    /// <typeparam name="T">以類型作為關聯的Key</typeparam>
    public abstract class InitializableObject<T> : MonoBehaviour, IInitializable where T : IInitializable {

        /// <summary>呼叫所有關聯物件更新資源</summary>
        public static void Initialize() {
            ResourceInitializer.Initialize(typeof(T));
        }

        protected virtual void Start() {
            ResourceInitializer.Add(typeof(T), this);
        }

        protected virtual void OnDestroy() {
            ResourceInitializer.Remove(typeof(T), this);
        }

        /// <summary>更新資源的方法</summary>
        public abstract void OnResInitialized();
    }
}