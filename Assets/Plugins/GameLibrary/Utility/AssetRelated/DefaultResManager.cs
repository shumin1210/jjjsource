﻿using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Utility {

    public struct DefaultResManager {

        public static Font DefaultFont {
            get {
                return DataShared.Instance.GetData<Font>(DEFAULT_FONT, true);
            }
            set {
                if (value) {
                    DataShared.Instance.SetData(DEFAULT_FONT, value, true);
                }
            }
        }

        public const string DEFAULT_FONT = "defaultFont";

        private static void setFont(Transform target, Font font) {
            Text txt = target.GetComponent<Text>();
            if (txt && txt.font == null) {
                txt.font = font;
            }
        }

        public static void SetFont(Component root) {
            if (root) {
                SetFont(root.transform, true);
            }
        }

        public static void SetFont(GameObject root) {
            if (root) {
                SetFont(root.transform, true);
            }
        }

        public static void SetFont(Transform root, bool searchChild = true) {
            if (!root) {
                return;
            }

            Font fnt = DataShared.Instance.GetData<Font>(DEFAULT_FONT, true);
            if (fnt) {
                setFont(root, fnt);
                if (searchChild) {
                    GameObjectRelate.SearchAllNodes(root, c => setFont(c, fnt));
                }
            }
        }
    }
}