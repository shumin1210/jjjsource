﻿using System;
using UnityEngine.EventSystems;

namespace GameLibrary.Utility {

    public static class EventTriggerExtension {

        public static void AddEventListener(this EventTrigger trigger, EventTriggerType eventID, Action<BaseEventData> callback) {
            EventTrigger.Entry entry = new EventTrigger.Entry {
                eventID = eventID,
                callback = new EventTrigger.TriggerEvent()
            };
            entry.callback.AddListener(data => callback(data));
            trigger.triggers.Add(entry);
        }
    }
}