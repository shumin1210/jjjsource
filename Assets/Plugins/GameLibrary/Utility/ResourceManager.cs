﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using GameLibrary.UI;

namespace GameLibrary.Utility {

    public class ResourceManager : MonoBehaviour {

        [Serializable]
        public struct SpriteAtlasKeyValyePair {
            public string Key;
            public Sprite[] Value;
        }

        public GameObject[] prefabs;
        public SpriteAtlasKeyValyePair[] sprites;
        public AudioClip[] audios;
        public TextAsset[] texts;

        private void Start() {
            DataShared.Instance.Prefabs.Load(prefabs);
            sprites.Foreach(s => DataShared.Instance.Sprites.SetAtlas(s.Key, s.Value));
            DataShared.Instance.Audios.Load(audios.ToArray());
            DataShared.Instance.TextAssets.Load(texts);

            InitializableObject<LocalizedText>.Initialize();
        }

        private void OnDestroy() {
            prefabs = null;
            sprites = null;
            audios = null;
        }
    }
}