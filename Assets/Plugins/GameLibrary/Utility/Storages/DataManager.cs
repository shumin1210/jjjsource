﻿using System.Collections.Generic;

namespace GameLibrary.Utility {

    public class DataManager : TDictionary<string, object> {
        public string seperator = ".";

        public new object this[string key] {
            get {
                return Get(key);
            }
            set {
                Set(key, value);
            }
        }

        public new object Get(string key) {
            string[] keys = key.Split(seperator);
            if (keys.Length > 1) {
                return Get(keys);
            } else {
                return base.Get(key);
            }
        }

        public object Get(string[] keys) {
            IDictionary<string, object> dic = this;
            for (int i = 0; i < keys.Length; i++) {
                if (dic == null) {
                    break;
                }
                bool isLast = i == keys.Length - 1;
                string key = keys[i];
                if (isLast && dic.ContainsKey(key)) {
                    return dic[key];
                } else if (dic.ContainsKey(key)) {
                    dic = dic[key] as IDictionary<string, object>;
                } else {
                    break;
                }
            }
            return null;
        }

        public new T Get<T>(string key) {
            string[] keys = key.Split(seperator);
            if (keys.Length > 1) {
                return Get<T>(keys);
            } else {
                return base.Get<T>(key);
            }
        }

        public T Get<T>(string[] keys) {
            return TConvert.ToGeneric<T>(Get(keys));
        }

        public void Set(string keys, object value) {
            Set(keys.Split(seperator), value);
        }

        public void Set(string[] keys, object value) {
            IDictionary<string, object> dic = this;
            for (int i = 0; i < keys.Length; i++) {
                if (dic == null) {
                    return;
                }
                bool isLast = i == keys.Length - 1;
                string key = keys[i];

                if (isLast) {
                    if (dic.ContainsKey(key)) {
                        dic[key] = value;
                    } else {
                        dic.Add(key, value);
                    }
                } else {
                    if (!dic.ContainsKey(key)) {
                        dic.Add(key, new TDictionary<string, object>());
                    }
                    dic = dic[key] as IDictionary<string, object>;
                }
            }
        }

        public void Remove(string[] keys) {
            IDictionary<string, object> dic = this;
            for (int i = 0; i < keys.Length; i++) {
                if (dic == null) {
                    return;
                }
                bool isLast = i == keys.Length - 1;
                string key = keys[i];

                if (isLast) {
                    if (dic.ContainsKey(key)) {
                        dic.Remove(key);
                    }
                } else {
                    if (dic.ContainsKey(key)) {
                        dic = dic[key] as IDictionary<string, object>;
                    } else {
                        return;
                    }
                }
            }
        }

        public new void Clear() {
            foreach (object item in this.Values) {
                clear(item as IDictionary<string, object>);
            }
            base.Clear();
        }

        private void clear(IDictionary<string, object> dic) {
            if (dic == null) {
                return;
            }
            foreach (object item in dic.Values) {
                if (item is IDictionary<string, object>) {
                    clear(item as IDictionary<string, object>);
                }
            }
            dic.Clear();
        }
    }
}