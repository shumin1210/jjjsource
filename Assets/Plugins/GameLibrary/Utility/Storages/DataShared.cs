﻿using System;
using UnityEngine;

namespace GameLibrary.Utility {

    public partial class DataShared : Singleton<DataShared> {
        private string gid;

        public string GID {
            get {
                return gid;
            }
        }

        public ResManager<AudioClip> Audios {
            get;
            private set;
        }

        public ResManager<GameObject> Prefabs {
            get;
            private set;
        }

        public ResManager<TextAsset> TextAssets {
            get;
            private set;
        }

        public SpriteResManager Sprites {
            get;
            private set;
        }

        public DataManager Data {
            get;
            private set;
        }

        public DataManager StaticData {
            get;
            private set;
        }

        public DataShared() {
            Data = new DataManager();
            StaticData = new DataManager();

            Audios = new ResManager<AudioClip>();
            Prefabs = new ResManager<GameObject>();
            Sprites = new SpriteResManager();
            TextAssets = new ResManager<TextAsset>();
        }

        public void SetGame(string GID) {
            if (gid != GID) {
                gid = GID;
                Clear();
            }
        }

        public void Clear(bool clearAll = false) {
            if (clearAll) {
                StaticData.Clear();
            }
            Data.Clear();

            Sprites.Clear();
            Prefabs.Clear();
            Audios.Clear();
            TextAssets.Clear();
        }

        #region DataRelated

        public void SetData(string keys, object value, bool isStatic = false) {
            (isStatic ? StaticData : Data).Set(keys, value);
        }

        public void SetData(string[] keys, object value, bool isStatic = false) {
            (isStatic ? StaticData : Data).Set(keys, value);
        }

        public bool ContainsKey(string key, bool isStatic = false) {
            return (isStatic ? StaticData : Data).ContainsKey(key);
        }

        public object GetData(string keys, bool isStatic = false) {
            if (string.IsNullOrEmpty(keys)) {
                return null;
            } else {
                return (isStatic ? StaticData : Data)[keys];
            }
        }

        public T GetData<T>(string keys, bool isStatic = false) {
            return (isStatic ? StaticData : Data).Get<T>(keys);
        }

        public object GetData(string[] keys, bool isStatic = false) {
            return (isStatic ? StaticData : Data).Get(keys);
        }

        #endregion DataRelated

        #region SpriteRelated

        [Obsolete("use Sprites.LoadAtlas(string root).", false)]
        public void LoadSpriteAtlas(string root, bool force = false) {
            Sprites.LoadAtlas(root);
        }

        [Obsolete("use Sprites.Set(string root,params atlas).", false)]
        public void SetSprite(string key, params Sprite[] atlas) {
            Sprites.SetAtlas(key, atlas);
        }

        [Obsolete("use Sprites.GetAtlas(string root).", false)]
        public ResManager<Sprite> GetAtlas(string root) {
            return Sprites.GetAtlas(root);
        }

        [Obsolete("use Sprites.GetAtlas(string root, string name)", false)]
        public Sprite GetSprite(string root, string name) {
            Sprites.GetAtlas(root).Get(name);
            return Sprites.GetAtlas(root)[name];
        }

        #endregion SpriteRelated

        #region AudioRelated

        [Obsolete("use Audio.Load(string res)", false)]
        public AudioClip LoadAudio(string res) {
            return Audios.Load(res);
        }

        [Obsolete("use Audio.Load(params string[] res)", false)]
        public AudioClip[] LoadAudios(params string[] res) {
            return Audios.Load(res);
        }

        [Obsolete("use Audio.Get(string res)", false)]
        public AudioClip GetAudio(string res) {
            return Audios[res];
        }

        #endregion AudioRelated
    }
}