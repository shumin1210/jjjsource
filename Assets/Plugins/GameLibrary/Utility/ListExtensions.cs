﻿using System.Collections.Generic;
using System.Linq;

namespace GameLibrary.Utility {

    /// <summary>清單類別擴充</summary>
    public static class ListExtensions {

        /// <summary>建立指定長度的清單物件</summary>
        /// <param name="length">清單長度</param>
        public static List<int> Build(int length) {
            return Build(length, 0, 1);
        }

        /// <summary>建立指定起始值及長度的遞增清單</summary>
        /// <param name="length">清單長度</param>
        /// <param name="start">起始元素值</param>
        public static List<int> Build(int length, int start) {
            return Build(length, start, 1);
        }

        /// <summary>建立清單</summary>
        /// <param name="length">清單長度</param>
        /// <param name="start">起始值</param>
        /// <param name="dif">元素間差值</param>
        public static List<int> Build(int length, int start, int dif) {
            List<int> ar = new List<int>();
            for (int i = 0, j = start; i < length; i++, j += dif) {
                ar.Add(j);
            }

            return ar;
        }

        /// <summary>嘗試加入元素(不重複)</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="src">目標清單物件</param>
        /// <param name="value">要加入的值</param>
        public static void TryAdd<T>(this List<T> src, T value) {
            if (!src.Contains(value)) {
                src.Add(value);
            }
        }

        /// <summary>移除第一個元素</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="src">目標清單物件</param>
        /// <returns>被移除的元素</returns>
        public static T Shift<T>(this List<T> src) {
            if (src.Count() > 0) {
                T result = src.ElementAt(0);
                src.RemoveAt(0);
                return result;
            }
            return default(T);
        }

        /// <summary>輸出以字串串接元素的字串</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="arr">目標集合物件</param>
        /// <param name="seperator">串接字串</param>
        public static string Join<T>(this IEnumerable<T> arr, string seperator) {
            return string.Join(seperator, arr.Select(s => s.ToString()).ToArray());
        }

        /// <summary>輸出以字串串接元素的字串</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="list">目標集合物件</param>
        /// <param name="seperator">串接字串</param>
        public static string Join<T>(this IList<T> list, string seperator) {
            return string.Join(seperator, list.Select(s => s.ToString()).ToArray());
        }

        /// <summary>回傳集合中的隨機元素</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="l">目標集合物件</param>
        public static T RandomElement<T>(this List<T> l) {
            return l.RandomElement(false);
        }

        /// <summary>回傳集合中的隨機元素</summary>
        /// <typeparam name="T">元素型別</typeparam>
        /// <param name="l">目標集合物件</param>
        /// <param name="remove">是否從集合中移除元素</param>
        public static T RandomElement<T>(this List<T> l, bool remove) {
            if (l.Count > 0) {
                int index = TRandom.Next(0, l.Count);
                T result = l.ElementAt(index);
                if (remove) {
                    l.RemoveAt(index);
                }

                return result;
            }
            return default(T);
        }
    }
}