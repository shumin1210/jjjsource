﻿using System.Collections.Generic;

namespace GameLibrary.Utility {

    /// <summary>型別擴充</summary>
    public class TType {

        /// <summary>是否為預設值</summary>
        /// <typeparam name="T">型別</typeparam>
        /// <param name="obj">要檢測的物件</param>
        /// <returns>是否為預設值</returns>
        public static bool isDefaultObject<T>(T obj) {
            if (obj == null) {
                return true;
            }

            if (typeof(T).IsValueType) {
                return false;
            }

            return EqualityComparer<T>.Default.Equals(obj, default(T));
        }
    }
}