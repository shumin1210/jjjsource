﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameLibrary.Utility {

    public class TableAnalyser {

        private List<string> columns;
        private List<List<object>> data;

        public TableAnalyser(List<string> columns, List<List<object>> data) {
            this.columns = columns;
            this.data = data;
        }

        public object GetData(int rowIndex,string columnName) {
            return data[rowIndex][columns.IndexOf(columnName)];
        }

        public T GetData<T>(int rowIndex, string columnName)  {
            return (T)Convert.ChangeType(GetData(rowIndex, columnName), typeof(T));
        }

    }
}