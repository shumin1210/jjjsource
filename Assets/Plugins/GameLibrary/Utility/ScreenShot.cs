﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenShot : MonoBehaviour {
#if UNITY_EDITOR
    [UnityEditor.MenuItem("Test/ScreenShot/TakePicture")]
    public static void TakePicture() {
        TakePicture(null);
    }

    [UnityEditor.MenuItem("Test/ScreenShot/SetRecordPage")]
    public static void TakePicturea() {
        TakePicture(s => {
            GameObject.Find("ScreenShot").GetComponent<Image>().sprite = s;
        });
    }
    [UnityEditor.MenuItem("Test/ScreenShot/TakePicture", validate = true)]
    [UnityEditor.MenuItem("Test/ScreenShot/SetRecordPage", validate = true)]
    public static bool canTakePicture() {
        return GameObject.FindObjectOfType<ScreenShot>() != null;
    }
#endif
    public static void TakePicture(Action<Sprite> callback = null) {
        snapPhoto = 1;
        ScreenShot.callback = callback;
    }

    private static Action<Sprite> callback;
    private static int snapPhoto = 0;
    public static Texture2D image {
        get; private set;
    }
    public static Sprite Photo {
        get; private set;
    }

    IEnumerator OnPostRender() {
        if (snapPhoto == 1) {
            yield return new WaitForEndOfFrame();
            image = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            image.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
            image.Apply();
            snapPhoto = 2;

            Photo = Sprite.Create(image, new Rect(0, 0, Screen.width, Screen.height), new Vector2(0, 0));
            if (callback != null) {
                callback(Photo);
            }
        }
    }
}