﻿using System;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;

namespace GameLibrary.Utility {

    /// <summary>String的擴充方法</summary>
    public static class TString {

        /// <summary>嘗試將物件轉換成字串</summary>
        /// <returns>若物件為Null或字串為空則返回false</returns>
        public static bool TryParse(object o, out string s) {
            s = "";
            if (o == null) {
                return false;
            }

            s = o.ToString();

            return string.IsNullOrEmpty(s);
        }

        /// <summary>檢測參數陣列中是否有包含null、空值或全空白字元(AND)</summary>
        public static bool ContainNulls(params string[] vals) {
            foreach (string str in vals) {
                if (string.IsNullOrEmpty(str)) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>檢測參數陣列中是否有包含非空白的字串(OR)</summary>
        public static bool ContainValues(params string[] vals) {
            foreach (string str in vals) {
                if (!string.IsNullOrEmpty(str)) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>以自串為分割子來分割字串</summary>
        /// <param name="target">來源字串</param>
        /// <param name="splitStr">分割子</param>
        /// <returns>分割後的字串陣列</returns>
        public static string[] Split(this string target, string splitStr) {
            return target.Split(new string[] { splitStr }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>以自串為分割子來分割字串</summary>
        /// <param name="target">來源字串</param>
        /// <param name="splitStr">分割子</param>
        /// <param name="regOpt">比對選項</param>
        /// <returns>分割後的字串陣列</returns>
        public static string[] Split(this string target, string splitStr, RegexOptions regOpt = RegexOptions.None) {
            return Regex.Split(target, splitStr, regOpt);
        }

        /// <summary>格式化字串</summary>
        /// <param name="str">格式字串</param>
        /// <param name="param">替換元素</param>
        /// <returns>以String.Format替換的字串</returns>
        public static string format(this String str, params object[] param) {
            return string.Format(str, param);
        }

        public static string Format(string pattern, object param) {
            if (param == null) {
                return "";
            }
            try {
                if (param.GetType().IsArray) {
                    var arr = (param as Array);
                    object[] result = new object[arr.Length];
                    for (int i = 0; i < arr.Length; i++) {
                        result[i] = TConvert.ToString(arr.GetValue(i));
                    }
                    return string.Format(pattern, args: result);
                } else if (param.GetType().IsGenericType) {
                    return string.Format(pattern, args: param as IEnumerable<object>);
                } else {
                    return string.Format(pattern, param);
                }
            } catch (Exception) {
                return "";
            }
        }

        public static string Format(string pattern, IEnumerable<object> param) {
            return string.Format(pattern, args: param.ToArray());
        }

        public static string Format(string pattern, Array param) {
            return string.Format(pattern, args: param);
        }
    }
}