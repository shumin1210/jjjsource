﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace GameLibrary.Utility {

    public class CSVReader {
        private string file;
        private bool hasHeader = false;
        private TDataTable datas = new TDataTable();

        public string GetData(int rowIndex, int colIndex) {
            return datas.GetData(rowIndex, colIndex).ToString();
        }

        public string GetData(int rowIndex, string colName) {
            return datas.GetData(rowIndex, colName).ToString();
        }

        public T GetData<T>(int rowIndex, int colIndex) where T : struct {
            return datas.GetData<T>(rowIndex, colIndex);
        }

        public T GetData<T>(int rowIndex, string colName) where T : struct {
            return datas.GetData<T>(rowIndex, colName);
        }

        public CSVReader(string file, bool hasHeader = false) {
            this.file = file;
            this.hasHeader = hasHeader;
            load();
        }

        public void Reload() {
            load();
        }

        private const string CSV_PATTERN = "(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)";

        private void load() {
            string[] columns = null;
            string[][] data = null;
            string src = Resources.Load<TextAsset>(file).text;
            string[] splited = src.Split("\r\n");
            IEnumerable<MatchCollection> matches = splited.Select(s => Regex.Matches(s, CSV_PATTERN));
            string[][] results = matches.Select(m => m.Select(n => Regex.Replace(n.Groups[1].Value, "^(\")|(\")$", "").Replace("\"\"", "")).ToArray()).ToArray();
            if (hasHeader) {
                columns = results[0];
                data = results.Skip(1).ToArray();
            } else {
                data = results;
            }
            datas.Load(columns, data);
        }
    }

    internal static class MatchCollectionExtension {

        public static IEnumerable<T> Select<T>(this MatchCollection matches, Func<Match, T> selector) {
            List<T> results = new List<T>();
            foreach (Match item in matches) {
                results.Add(selector(item));
            }
            return results;
        }
    }
}