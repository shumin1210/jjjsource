﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameLibrary.Utility {

    public static class AudioSourceExtension {

        public static void PlayClip(this AudioSource src, string res) {
            src.PlayClip(DataShared.Instance.LoadAudio(res));
        }

        public static void PlayClip(this AudioSource src, string res, bool loop) {
            src.PlayClip(DataShared.Instance.LoadAudio(res), loop);
        }

        public static void PlayClip(this AudioSource src, AudioClip clip) {
            src.clip = clip;
            src.loop = false;
            src.Play();
        }

        public static void PlayClip(this AudioSource src, AudioClip clip, bool loop) {
            src.loop = loop;
            PlayClip(src, clip);
        }
    }
}