# Unity2D遊戲開發框架

## 標記
* {Game} = 同遊戲名稱字串

## 專案資料夾結構

    Project
    +---Assets          -資源
    +---AssetBundles    -資源包
    +---Output          -輸出路徑
        +---andriod         -輸出apk
        +---ios             -輸出xcode(在mac上才需要)
        +---webgl           -輸出webgl網頁

## Assets資料夾結構

    Assets
    +---Editor          -編輯器相關
    +---Plugins         -GameLibrary等套件
    +---Res             -遊戲使用的所有資源
    |   +---Images          -圖片資源
    |   +---Audios          -音樂資源
    |   +---Prefabs         -prefab資源
    +---Scripts         -指令碼
    |   +---{Game}          -遊戲相關指令碼(與大廳相同格式才能被正確使用)
    +---Shared          -共用資源區
    (以下視情況可選)    
    +---Resources       -預設資源,預設輸出時會強制合併到安裝檔的資源可以放在此
    +---UniversalMediaPlayer    -通用媒體播放套件(修改結構會造成錯誤,故不強制移動)

### 資源使用
* 極小化使用Resource資料夾,及Resource.Load或直接取用資源路徑的相關方法,  
    將資源放在Assets/Res資料夾底下,
    並可配合ResourceManager使用資源.
* 除了基礎資源以外,其餘資源皆使用AssetBundle方式取用
* Assets/Shared存放通用prefabs(例:Loading.prefab,msjh.ttf)

## 場景結構

    Root  
    +---MainCamera
    +---MainCanvas
    +---EventSystem
        +---{Game}View
    (以下視情況可選)
        +---Loading    
        +---[其他通用View]
    
## 基本設定:

| 物件 | 說明 | Components | properties |  
|------|------|------------|-------|
|**MainCamera**|預設攝影機物件 |**Camera** |   |
||||Projection = Orthographic||
||||Size = 5||
|**EventSystem**|預設事件處理物件|(default)||
|**MainCanvas**|主要Canvas物件|**Canvas**| |
||||RenderMode = ScreenSpace-Camera||
||||PixelPerfect = true||
|||**CanvasScaler**||
||||UIScaleMode = ScaleWithScreenSize||
||||ReferenceResolution = (1366,768)||
||||ScreenMatchMode = MatchWidthOrHeight||
||||Match = 0.5
||||ReferencePixelPerUnit = 100
|**{Game}View**|遊戲畫面根目錄||Prefab = {Game}View|
||||AssetBundle = {Game}|

### MainCanvas-畫面根目錄
* 所有2D物件皆附屬在此物件下
* 除了{Game}View以外,其他通用的View資源皆附屬於此物件下,建議維持各通用資源原始配置及結構.

### {Game}View-遊戲根目錄
* Connection
    + 使用websocket的通訊接口
* {Game}Handler : MsgHandler
    + auto/manual ref Connection
    + 實作通訊處理
* (opt)ResourceManager
    + 資源彙整管理,同步至DataShared
* 所有相關{Game}的項目皆附屬在此物件下
