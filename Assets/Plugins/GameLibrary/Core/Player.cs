﻿namespace GameLibrary.Core {

    public class Player {
        public string Name;
        public string Token;
        public long Credit;
        public long Refund;
        public bool IsOverLimit;
        public int AccessLevel;
    }
}