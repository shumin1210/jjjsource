﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameLibrary.Core {

    public class Core : MonoBehaviour {
#if UNITY_EDITOR

        [UnityEditor.MenuItem("GameLibrary/SetCameraProps")]
        public static void SetCameraDefault() {
            var go = UnityEditor.Selection.activeGameObject;
            if (go) {
                SetCameraProperties(go.GetComponent<Camera>());
            }
        }

#endif

        public static void SetCameraProperties(Camera camera) {
            if (camera) {
                camera.orthographic = true;
                camera.orthographicSize = 5;
                camera.depth = 2;
                camera.farClipPlane = 1000;
                camera.nearClipPlane = .3f;
                Debug.Log("Camera is modified.");
            } else {
                Debug.Log("Camera is null.");
            }
        }

        public static void SetCanvasVPRoperties(GameObject go) {
            var canvas = go.GetComponent<Canvas>();
            var canvasScaler = go.GetComponent<CanvasScaler>();

            if (canvas && canvasScaler) {
                canvas.renderMode = RenderMode.ScreenSpaceCamera;
                canvas.pixelPerfect = true;
                canvas.planeDistance = 100;
                canvas.additionalShaderChannels =
                    AdditionalCanvasShaderChannels.TexCoord1
                    | AdditionalCanvasShaderChannels.Normal
                    | AdditionalCanvasShaderChannels.Tangent;

                canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
                canvasScaler.referenceResolution = new Vector2(1366, 768);
                canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
                canvasScaler.matchWidthOrHeight = .5f;
                canvasScaler.referencePixelsPerUnit = 100;

                Debug.Log("Canvas is modified.");
            } else {
                Debug.Log("Canvas is null.");
            }
        }
    }
}