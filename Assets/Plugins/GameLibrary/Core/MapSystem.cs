﻿using UnityEngine;

namespace GameLibrary.Core {

    /// 地圖系統</summary>
    public class MapSystem : Singleton<MapSystem> {

        /// <summary>地圖單元</summary>
        public struct Tile {

            /// <summary>單元類型</summary>
            public int Type;

            /// <summary>單元等級</summary>
            public int Level;

            /// <summary>單元觸發效果</summary>
            public int Effect;
        }

        /// <summary>地圖</summary>
        public class Map {

            /// <summary>單元集合</summary>
            protected Tile[,,] Tiles;

            /// <summary>建構式,建立指定大小的地圖,可建立2/3維地圖</summary>
            /// <param name="mapSize">地圖大小</param>
            public Map(Vector3 mapSize) {
                Tiles = new Tile[(int)mapSize.x, (int)mapSize.z, (int)mapSize.y];
            }

            /// <summary>建構式,讀取地圖資源</summary>
            /// <param name="mapName">資源路徑</param>
            public Map(string mapName) {
            }

            /// <summary>透過指定點座標,取得目標地圖單元資料</summary>
            /// <param name="point">指定座標點</param>
            /// <returns>目標地圖單元資料</returns>
            public Tile GetCell(Vector3 point) {
                return Tiles[(int)point.x, (int)point.y, (int)point.z];
            }

            /// <summary>讀取地圖資源</summary>
            /// <param name="mapName">資源路徑</param>
            public void Load(string mapName) { }

            /// <summary>儲存地圖配置</summary>
            public void Save() { }
        }

        /// <summary>目前使用中的地圖資料</summary>
        private Map map;

        /// <summary>讀取地圖</summary>
        public void LoadMap() { }

        /// <summary>基於cell座標點,面相direction,觸發地圖單元效果/事件</summary>
        /// <param name="cell">來源座標</param>
        /// <param name="direction">觸發方向</param>
        public void InterAct(Vector3 cell, Vector3 direction) { }
    }
}